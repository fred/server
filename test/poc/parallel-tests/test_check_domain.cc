/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/backend/epp/domain/check_domain.hh"

#include "test/poc/parallel-tests/fixtures/contact.hh"
#include "test/poc/parallel-tests/fixtures/domain.hh"
#include "test/poc/parallel-tests/fixtures/has_fresh_database.hh"
#include "test/poc/parallel-tests/fixtures/operation_context.hh"
#include "test/poc/parallel-tests/fixtures/registrar.hh"
#include "test/poc/parallel-tests/fixtures/zone.hh"
#include "test/poc/parallel-tests/fixtures/zone_access.hh"

#include "libfred/registrable_object/domain/auction/create_auction.hh"
#include "libfred/registrable_object/domain/auction/update_auction.hh"

#include "liblog/liblog.hh"

#include <boost/test/unit_test.hpp>

#include <utility>


namespace Test {

BOOST_AUTO_TEST_SUITE(ProofOfConcept)
BOOST_AUTO_TEST_SUITE(Parallel)
BOOST_AUTO_TEST_SUITE(CheckDomain)

namespace {

struct RollbackingOperationContextFixture : HasFreshDatabase
{
    RollbackingOperationContextFixture()
        : HasFreshDatabase{},
          ctx{},
          registrar{ctx, Setter::registrar(LibFred::CreateRegistrar{
                "REG-A",
                "Name A",
                "Organization A",
                {"street A"},
                "City A",
                "CZ",
                "a@gmail.com",
                "https://organization-a.com",
                "--dic-a--"})}
    { }
    OperationContext ctx;
    Registrar registrar;
};

struct EnableAuction
{
    explicit EnableAuction(const LibFred::OperationContext& ctx, const Zone& zone)
    {
        ctx.get_conn().exec_params(
                "UPDATE zone SET auction_enabled = True "
                 "WHERE fqdn = $1::TEXT",
                Database::QueryParams{zone.data.fqdn});
    }
};

struct HasCzZoneFixture : HasFreshDatabase
{
    HasCzZoneFixture()
        : HasFreshDatabase{},
          ctx{},
          cz_zone{ctx},
          registrar{ctx, Setter::registrar(LibFred::CreateRegistrar{
                "REG-A",
                "Name A",
                "Organization A",
                {"street A"},
                "City A",
                "CZ",
                "a@gmail.com",
                "https://organization-a.com",
                "--dic-a--"})},
          zone_access{ctx, cz_zone, registrar},
          enable_auction{ctx, cz_zone}
    { }
    OperationContext ctx;
    CzZone cz_zone;
    Registrar registrar;
    ZoneAccess zone_access;
    EnableAuction enable_auction;
};

struct Session : ::Epp::SessionData
{
    explicit Session(const Registrar& registrar)
        : ::Epp::SessionData{
                  registrar.data.id,
                  ::Epp::SessionLang::en,
                  "",
                  boost::make_optional(0ull)}
    { }
};

struct DefaultCheckDomainConfigData : ::Epp::Domain::CheckDomainConfigData
{
    DefaultCheckDomainConfigData()
        : CheckDomainConfigData{false}//rifd_epp_operations_charging
    { }
};

struct HasSessionAndAuctionFixture : HasCzZoneFixture
{
    HasSessionAndAuctionFixture()
        : session{registrar},
          winner{ctx, Setter::contact(LibFred::CreateContact{"WINNER", registrar.data.handle})},
          not_a_winner{ctx, Setter::contact(LibFred::CreateContact{"NOT-A-WINNER", registrar.data.handle})},
          fqdn{"auctioned.cz"}
    {
        const auto auction_id = ::LibFred::Domain::Auction::CreateAuction{fqdn}.exec(ctx);
        ::LibFred::Domain::Auction::UpdateAuction{auction_id}.set_external_auction_id(
                ::LibFred::Domain::Auction::ExternalAuctionId{"external-auction-id"}).exec(ctx);
        ::LibFred::Domain::Auction::UpdateAuction{auction_id}
                .set_winner_id(winner.data.id)
                .set_win_expires_at(std::chrono::system_clock::now() + std::chrono::hours{24})
                .exec(ctx);
    }
    Session session;
    Contact winner;
    Contact not_a_winner;
    std::string fqdn;
};

}//namespace Test::{anonymous}

BOOST_FIXTURE_TEST_CASE(test_winner_ok, HasSessionAndAuctionFixture)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    const auto check_domain_res = ::Epp::Domain::check_domain(
            ctx,
            {fqdn},
            winner.data.handle,
            DefaultCheckDomainConfigData(),
            session);
    BOOST_REQUIRE_EQUAL(check_domain_res.size(), 1);
    BOOST_CHECK_EQUAL(check_domain_res.begin()->first, fqdn);
    BOOST_CHECK(check_domain_res.begin()->second.isnull());
}

BOOST_FIXTURE_TEST_CASE(test_not_a_winner, HasSessionAndAuctionFixture)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    {
        const auto check_domain_res = ::Epp::Domain::check_domain(
                ctx,
                {fqdn},
                not_a_winner.data.handle,
                DefaultCheckDomainConfigData(),
                session);
        BOOST_REQUIRE_EQUAL(check_domain_res.size(), 1);
        BOOST_CHECK_EQUAL(check_domain_res.begin()->first, fqdn);
        BOOST_REQUIRE(!check_domain_res.begin()->second.isnull());
        BOOST_CHECK(check_domain_res.begin()->second.get_value() == Epp::Domain::DomainRegistrationObstruction::not_a_winner);
    }
    {
        const auto check_domain_res = ::Epp::Domain::check_domain(
                ctx,
                {fqdn},
                boost::none,
                DefaultCheckDomainConfigData(),
                session);
        BOOST_REQUIRE_EQUAL(check_domain_res.size(), 1);
        BOOST_CHECK_EQUAL(check_domain_res.begin()->first, fqdn);
        BOOST_REQUIRE(!check_domain_res.begin()->second.isnull());
        BOOST_CHECK(check_domain_res.begin()->second.get_value() == Epp::Domain::DomainRegistrationObstruction::not_a_winner);
    }
}

BOOST_FIXTURE_TEST_CASE(test_not_a_contact, HasSessionAndAuctionFixture)
{
    LIBLOG_SET_CONTEXT(LogCtx, log_ctx, boost::unit_test::framework::current_test_case().p_name);
    BOOST_CHECK_EXCEPTION(::Epp::Domain::check_domain(
            ctx,
            {fqdn},
            std::string{"NOT-A-CONTACT"},
            DefaultCheckDomainConfigData(),
            session),
            Epp::Domain::RegistrantDoesNotExist,
            [](auto&& e) { LIBLOG_INFO("exception caught: {}", e.what()); return true; });
}

BOOST_AUTO_TEST_SUITE_END()//ProofOfConcept/Parallel/CheckDomain
BOOST_AUTO_TEST_SUITE_END()//ProofOfConcept/Parallel
BOOST_AUTO_TEST_SUITE_END()//ProofOfConcept

}//namespace Test
