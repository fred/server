/*
 * Copyright (C) 2016-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "test/backend/epp/contact/fixture.hh"
#include "test/backend/epp/util.hh"
#include "test/setup/fixtures.hh"

#include "src/backend/epp/impl/conditionally_enqueue_notification.hh"
#include "src/backend/epp/notification_data.hh"
#include "src/backend/epp/session_data.hh"
#include "src/backend/epp/session_lang.hh"

#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_SUITE(TestEpp)
BOOST_AUTO_TEST_SUITE(ConditionallyEnqueueNotification)

struct FailedToLockRequest { };

template<bool SystemReg>struct has_updated_contact_and_empty_notification_queue : virtual Test::instantiate_db_template {
    unsigned long long post_update_contact_history_id;
    unsigned long long registrar_id;
    static const bool epp_notification_disabled = true;
    static const bool epp_notification_enabled = false;
    static const Epp::SessionLang::Enum epp_session_lang_default = Epp::SessionLang::en;

    has_updated_contact_and_empty_notification_queue()
    {
        ::LibFred::OperationContextCreator ctx;

        const std::string reg_handle = "REGISTRAR1";
        ::LibFred::CreateRegistrar(
                reg_handle,
                "Name",
                "Org",
                {"Street"},
                "City",
                "CZ",
                "e@ma.il",
                "https://u.rl",
                "--dic--",
                SystemReg).exec(ctx);
        const ::LibFred::InfoRegistrarData registrar = ::LibFred::InfoRegistrarByHandle(reg_handle).exec(ctx).info_registrar_data;
        registrar_id = registrar.id;

        const std::string contact_handle = "CONTACT1";
        ::LibFred::CreateContact(contact_handle, registrar.handle).exec(ctx);
        const ::LibFred::InfoContactData contact = ::LibFred::InfoContactByHandle(contact_handle).exec(ctx).info_contact_data;

        post_update_contact_history_id = ::LibFred::UpdateContactByHandle(contact.handle, registrar.handle)
            .set_email(contact.email)
            .exec(ctx);

        ctx.get_conn().exec("DELETE FROM notification.notification_queue");

        ctx.commit_transaction();
    }
};

typedef has_updated_contact_and_empty_notification_queue<true> has_system_registrar_updated_contact_and_empty_notification_queue;
typedef has_updated_contact_and_empty_notification_queue<false> has_nonsystem_registrar_updated_contact_and_empty_notification_queue;

BOOST_FIXTURE_TEST_CASE(notification_created, has_nonsystem_registrar_updated_contact_and_empty_notification_queue)
{
    Epp::conditionally_enqueue_notification(
            Notification::updated,
            post_update_contact_history_id,
            Epp::SessionData(
                    registrar_id,
                    epp_session_lang_default,
                    "srv-trx-007",
                    0ULL),
            Epp::NotificationData(
                    "cl-trx-007",
                    epp_notification_enabled,
                    "somethingElseAndNotMatching"));

    ::LibFred::OperationContextCreator ctx;

    BOOST_CHECK(static_cast<unsigned long long>(ctx.get_conn().exec("SELECT COUNT(*) FROM notification.notification_queue WHERE change = 'updated'::notification.notified_event")[0][0]) > 0);

    ctx.commit_transaction();
}

BOOST_FIXTURE_TEST_CASE(notification_created_because_of_nonsystem_registrar, has_nonsystem_registrar_updated_contact_and_empty_notification_queue)
{
    Epp::conditionally_enqueue_notification(
            Notification::updated,
            post_update_contact_history_id,
            Epp::SessionData(
                    registrar_id,
                    epp_session_lang_default,
                    "srv-trx-007",
                    0ULL),
            Epp::NotificationData(
                    "DOnotNOTIFY-cl-trx-007",
                    epp_notification_enabled,
                    "DOnotNOTIFY"));

    ::LibFred::OperationContextCreator ctx;

    BOOST_CHECK(static_cast<unsigned long long>(ctx.get_conn().exec("SELECT COUNT(*) FROM notification.notification_queue WHERE change = 'updated'::notification.notified_event")[0][0]) > 0);

    ctx.commit_transaction();
}

BOOST_FIXTURE_TEST_CASE(notification_created_because_of_nonmatching_prefix, has_system_registrar_updated_contact_and_empty_notification_queue)
{
    Epp::conditionally_enqueue_notification(
            Notification::updated,
            post_update_contact_history_id,
            Epp::SessionData(
                    registrar_id,
                    epp_session_lang_default,
                    "srv-trx-007",
                    0ULL),
            Epp::NotificationData(
                    "DOnotNOTIFY-cl-trx-007",
                    epp_notification_enabled,
                    "somethingElseAndNotMatching"));

    ::LibFred::OperationContextCreator ctx;

    BOOST_CHECK(static_cast<unsigned long long>(ctx.get_conn().exec("SELECT COUNT(*) FROM notification.notification_queue WHERE change = 'updated'::notification.notified_event")[0][0]) > 0);

    ctx.commit_transaction();
}

BOOST_FIXTURE_TEST_CASE(notification_not_created_because_of_sys_reg_and_prefix, has_system_registrar_updated_contact_and_empty_notification_queue)
{
    Epp::conditionally_enqueue_notification(
            Notification::updated,
            post_update_contact_history_id,
            Epp::SessionData(
                    registrar_id,
                    epp_session_lang_default,
                    "srv-trx-007",
                    0ULL),
            Epp::NotificationData(
                    "DOnotNOTIFY-cl-trx-007",
                    epp_notification_enabled,
                    "DOnotNOTIFY"));

    ::LibFred::OperationContextCreator ctx;

    BOOST_CHECK(static_cast<unsigned long long>(ctx.get_conn().exec("SELECT COUNT(*) FROM notification.notification_queue WHERE change = 'updated'::notification.notified_event")[0][0]) == 0);

    ctx.commit_transaction();
}

BOOST_FIXTURE_TEST_CASE(notification_not_created_because_of_config, has_nonsystem_registrar_updated_contact_and_empty_notification_queue)
{
    Epp::conditionally_enqueue_notification(
            Notification::updated,
            post_update_contact_history_id,
            Epp::SessionData(
                    registrar_id,
                    epp_session_lang_default,
                    "srv-trx-007",
                    0ULL),
            Epp::NotificationData(
                    "DOnotNOTIFY-cl-trx-007",
                    epp_notification_disabled,
                    "somethingElseAndNotMatching"));

    ::LibFred::OperationContextCreator ctx;

    BOOST_CHECK(static_cast<unsigned long long>(ctx.get_conn().exec("SELECT COUNT(*) FROM notification.notification_queue WHERE change = 'updated'::notification.notified_event")[0][0]) == 0);

    ctx.commit_transaction();
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();
