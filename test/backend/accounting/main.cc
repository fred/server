/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE TestAccounting

#include "config.h"

#include "test/setup/fixtures.hh"

#include "util/log/log.hh"

#include "src/util/cfg/config_handler_decl.hh"
#include "src/util/cfg/config_handler.hh"
#include "src/util/cfg/handle_tests_args.hh"
#include "src/util/cfg/handle_server_args.hh"
#include "src/util/cfg/handle_logging_args.hh"
#include "src/util/cfg/handle_database_args.hh"
#include "src/util/cfg/handle_corbanameservice_args.hh"
#include "src/util/cfg/handle_threadgroup_args.hh"
#include "src/util/cfg/handle_registry_args.hh"

#include <boost/assign/list_of.hpp>
#include <boost/test/framework.hpp>
#include <boost/test/unit_test.hpp>

namespace Test {

class HandleCommandLineArgs
{
public:
    HandleCommandLineArgs()
        : config_handlers(
                  static_cast<const HandlerPtrVector&>(
                          boost::assign::list_of
                                (HandleArgsPtr(new HandleTestsArgs(CONFIG_FILE)))
                                (HandleArgsPtr(new HandleServerArgs))
                                (HandleArgsPtr(new HandleLoggingArgs))
                                (HandleArgsPtr(new HandleDatabaseArgs))
                                (HandleArgsPtr(new HandleCorbaNameServiceArgs))
                                (HandleArgsPtr(new HandleThreadGroupArgs))
                                (HandleArgsPtr(new HandleRegistryArgs))
                                (HandleArgsPtr(new HandleAdminDatabaseArgs))
                                .convert_to_container<HandlerPtrVector>()))
    {
        CfgArgs::init<HandleTestsArgs>(config_handlers)->handle(
                boost::unit_test::framework::master_test_suite().argc,
                boost::unit_test::framework::master_test_suite().argv);
        setup_logging(CfgArgs::instance());
    }
private:
    static void setup_logging(CfgArgs *cfg_instance_ptr);
    const HandlerPtrVector config_handlers;
};

void HandleCommandLineArgs::setup_logging(CfgArgs* cfg_instance_ptr)
{
    ::setup_logging(*(cfg_instance_ptr->get_handler_ptr_by_type<HandleLoggingArgs>()));
}

} // namespace Test

class GlobalFixture
{
    Test::HandleCommandLineArgs handle_cmd_line_args;
    Test::create_db_template create_db_template;
};

BOOST_GLOBAL_FIXTURE(GlobalFixture);
