/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/setup_server.hh"
#include "src/util/corba_wrapper.hh"

#include "src/util/cfg/handle_server_args.hh"
#include "src/util/cfg/handle_corbanameservice_args.hh"

#include "src/bin/corba/daemonize.hh"
#include "src/bin/corba/pidfile.hh"

#include <unistd.h>

void run_server(CfgArgs* cfg_instance_ptr , CorbaContainer* corba_instance_ptr)
{
    corba_instance_ptr->poa_persistent->the_POAManager()->activate();

    //run server
    if (cfg_instance_ptr->get_handler_ptr_by_type<HandleServerArgs>()->do_daemonize)
    {
        daemonize();
    }
    const auto pidfile_name = cfg_instance_ptr->get_handler_ptr_by_type<HandleServerArgs>()->pidfile_name;

    if (!pidfile_name.empty())
    {
        PidFileNS::PidFileS::writePid(::getpid(), pidfile_name);
    }

    corba_instance_ptr->orb->run();
}

void run_server(CfgArgGroups* cfg_instance_ptr , CorbaContainer* corba_instance_ptr)
{
    corba_instance_ptr->poa_persistent->the_POAManager()->activate();

    //run server
    if (cfg_instance_ptr->get_handler_ptr_by_type<HandleServerArgsGrp>()->get_do_daemonize())
    {
        daemonize();
    }
    const auto pidfile_name = cfg_instance_ptr->get_handler_ptr_by_type<HandleServerArgsGrp>()->get_pidfile_name();

    if (!pidfile_name.empty())
    {
        PidFileNS::PidFileS::writePid(::getpid(), pidfile_name);
    }

    corba_instance_ptr->orb->run();
}

void corba_init()
{
    FakedArgs orb_fa = CfgArgs::instance()->fa;
    const auto* ns_args_ptr = CfgArgs::instance()->get_handler_ptr_by_type<HandleCorbaNameServiceArgs>();

    CorbaContainer::set_instance(
            orb_fa.get_argc(),
            orb_fa.get_argv(),
            ns_args_ptr->nameservice_host,
            ns_args_ptr->nameservice_port,
            ns_args_ptr->nameservice_context);
}


void corba_grpinit()
{
    FakedArgs orb_fa = CfgArgGroups::instance()->fa;
    const auto* ns_args_ptr = CfgArgGroups::instance()->get_handler_ptr_by_type<HandleCorbaNameServiceArgsGrp>();

    CorbaContainer::set_instance(
            orb_fa.get_argc(),
            orb_fa.get_argv(),
            ns_args_ptr->get_nameservice_host(),
            ns_args_ptr->get_nameservice_port(),
            ns_args_ptr->get_nameservice_context());
}
