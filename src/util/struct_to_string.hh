/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef STRUCT_TO_STRING_HH_DA69B6DC7C1F1B77122978EF54626C89
#define STRUCT_TO_STRING_HH_DA69B6DC7C1F1B77122978EF54626C89

#include "util/db/nullable.hh"
#include <string>

namespace Util {

class StructToString
{
public:
    StructToString();

    StructToString& add(const std::string& name, const std::string& value);

    template <typename T>
    StructToString& add(const std::string& name, const T& value)
    {
        using std::to_string;
        return _add(name, to_string(value));
    }

    template <typename T>
    StructToString& add(const std::string& name, const Nullable<T>& value)
    {
        if (value.isnull())
        {
            return _add(name, std::string{"[null]"});
        }
        else
        {
            return add(name, value.get_value());
        }
    }

    std::string finish();
private:
    StructToString& _add(const std::string& name, const std::string& value);

    std::string buffer_;
};

} // namespace Util

#endif
