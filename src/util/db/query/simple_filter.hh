/*
 * Copyright (C) 2008-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMPLE_FILTER_HH_3A54D23386174785806ADA40F04C9ACE
#define SIMPLE_FILTER_HH_3A54D23386174785806ADA40F04C9ACE

#include "src/util/db/query/sql_operators.hh"
#include "src/util/db/query/filter.hh"

#include <boost/serialization/nvp.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/utility.hpp>
#include <boost/serialization/version.hpp>

#include <string>

namespace Database {

class SelectQuery;

namespace Filters {

class Simple : public Filter
{
public:
    Simple()
        : Filter(SQL_OP_AND),
          allowed_wildcard(true)
    { }
  
    Simple(const std::string& _conj)
        : Filter(_conj),
          allowed_wildcard(true)
    { }
  
    Simple(const Simple &s) = default;

    virtual ~Simple();

    bool isSimple() const override { return true; }

    bool isActive() const override
    {
        FREDLOG_TRACE("[CALL] Simple::isActive()");
        FREDLOG_DEBUG(boost::format("filter '%1%' is %2%") % getName()
            % (active ? "active" : "not active"));
        return active;
    }

    virtual void enableWildcardExpansion() { allowed_wildcard = true; }

    virtual void disableWildcardExpansion() { allowed_wildcard = false; }
  
    void serialize(Database::SelectQuery&, const Settings*) override { }
  
    virtual void addPostValueString(const std::string& _str) { value_post_ = _str; }

    virtual void addPreValueString(const std::string& _str) { value_pre_ = _str; }
  
    template<class Archive>
    void serialize(Archive& _ar, const unsigned int)
    {
        _ar & BOOST_SERIALIZATION_BASE_OBJECT_NVP(Filter);
        _ar & BOOST_SERIALIZATION_NVP(allowed_wildcard);
        _ar & BOOST_SERIALIZATION_NVP(value_post_);
        _ar & BOOST_SERIALIZATION_NVP(value_pre_);
    }
protected:
    bool allowed_wildcard;
    std::string value_post_;
    std::string value_pre_;
private:
    friend class boost::serialization::access;
};

}//namespace Database::Filters
}//namespace Database

#endif//SIMPLE_FILTER_HH_3A54D23386174785806ADA40F04C9ACE
