/*
 * Copyright (C) 2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/util/cfg/handle_whois_args.hh"

HandleWhoisArgs::~HandleWhoisArgs()
{
}

std::shared_ptr<boost::program_options::options_description> HandleWhoisArgs::get_options_description()
{
    auto opts_descs = std::make_shared<boost::program_options::options_description>(
            std::string("Whois interface configuration"));
    opts_descs->add_options()
            ("whois.enable_inversekey_wildcard_expansion",
             boost::program_options::value<bool>()->default_value(true),
             "turn on/off using wildcards in inverse key search");

    return opts_descs;
}

void HandleWhoisArgs::handle(int argc, char* argv[], FakedArgs &fa)
{
    boost::program_options::variables_map vm;
    handler_parse_args()(get_options_description(), vm, argc, argv, fa);

    enable_inversekey_wildcard_expansion = vm["whois.enable_inversekey_wildcard_expansion"].as<bool>();
}
