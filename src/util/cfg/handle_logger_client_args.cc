/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/cfg/handle_logger_client_args.hh"

#include "libhermes/connection.hh"

#include <boost/numeric/conversion/cast.hpp>

#include <string>

std::shared_ptr<boost::program_options::options_description>
HandleLoggerClientArgs::get_options_description()
{
    auto opts_descs = std::make_shared<boost::program_options::options_description>("logger client options");
    opts_descs->add_options()
         ("logger_client", "logger client options")
         ("logger_client.corba_ns_logger_name", boost::program_options::value<std::string>()->default_value("Logger"),
             "Name of the logger in Corba NS which will be used for logging");
    return opts_descs;
}

void HandleLoggerClientArgs::handle(int argc, char* argv[], FakedArgs& fa)
{
    boost::program_options::variables_map vm;
    handler_parse_args()(get_options_description(), vm, argc, argv, fa);

    logger_client_args.corba_ns_logger_name = vm["logger_client.corba_ns_logger_name"].as<std::string>();
}

std::shared_ptr<boost::program_options::options_description>
HandleLoggerClientArgsGrp::get_options_description()
{
    return logger_client_args_.get_options_description();
}

std::size_t HandleLoggerClientArgsGrp::handle(int argc, char* argv[], FakedArgs& fa, std::size_t option_group_index)
{
    logger_client_args_.handle(argc, argv, fa);
    return option_group_index;
}

const LoggerClientArgs& HandleLoggerClientArgsGrp::get_args() const
{
    return logger_client_args_.logger_client_args;
}

const std::string& HandleLoggerClientArgsGrp::get_corba_ns_logger_name() const
{
    return logger_client_args_.logger_client_args.corba_ns_logger_name;
}

