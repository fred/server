/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HANDLE_LOGGER_CLIENT_ARGS_HH_BC7FC7AC106C45A7ABF442F706F41AB8
#define HANDLE_LOGGER_CLIENT_ARGS_HH_BC7FC7AC106C45A7ABF442F706F41AB8

#include "src/bin/cli/logger_client_params.hh"
#include "src/util/cfg/faked_args.hh"
#include "src/util/cfg/handle_args.hh"

#include <boost/program_options.hpp>

#include <string>

struct HandleLoggerClientArgs : HandleArgs
{
    LoggerClientArgs logger_client_args;

    std::shared_ptr<boost::program_options::options_description> get_options_description() override;
    void handle(int argc, char* argv[], FakedArgs& fa) override;
};

class HandleLoggerClientArgsGrp : public HandleGrpArgs
{
public:
    std::shared_ptr<boost::program_options::options_description> get_options_description() override;
    std::size_t handle(int argc, char* argv[], FakedArgs& fa, std::size_t option_group_index) override;
    const LoggerClientArgs& get_args() const;
    const std::string& get_corba_ns_logger_name() const;
private:
    HandleLoggerClientArgs logger_client_args_;
};

#endif
