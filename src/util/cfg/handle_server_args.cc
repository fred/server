/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/cfg/handle_server_args.hh"

std::shared_ptr<boost::program_options::options_description>
HandleServerArgsGrp::get_options_description()
{
    return server_args_.get_options_description();
}

std::size_t HandleServerArgsGrp::handle(int argc, char* argv[], FakedArgs& fa, std::size_t option_group_index)
{
    server_args_.handle(argc, argv, fa);
    return option_group_index;
}

bool HandleServerArgsGrp::get_do_daemonize() const
{
    return server_args_.do_daemonize;
}

const std::string& HandleServerArgsGrp::get_pidfile_name() const
{
    return server_args_.pidfile_name;
}
