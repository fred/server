/*
 * Copyright (C) 2010-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef HANDLE_LOGGING_ARGS_HH_431EDB4A1D714CE3BD4948CB99DFA42C
#define HANDLE_LOGGING_ARGS_HH_431EDB4A1D714CE3BD4948CB99DFA42C

#include "src/util/cfg/faked_args.hh"
#include "src/util/cfg/handle_args.hh"

#include "util/log/log.hh"

#include "liblog/log.hh"
#include "liblog/sink/console_sink_config.hh"
#include "liblog/sink/file_sink_config.hh"
#include "liblog/sink/syslog_sink_config.hh"

#include <boost/program_options.hpp>

#include <syslog.h>

#include <iostream>
#include <memory>
#include <exception>
#include <string>
#include <vector>

/**
 * \class HandleLoggingArgs
 * \brief logging options handler
 */
class HandleLoggingArgs : public HandleArgs
{
public:
    std::shared_ptr<boost::program_options::options_description>
    get_options_description()
    {
        std::shared_ptr<boost::program_options::options_description> cfg_opts{
                new boost::program_options::options_description{"Logging configuration"}};
        cfg_opts->add_options()
                ("log.type", boost::program_options
                        ::value<unsigned int>()->default_value(1), "log type: 0-console, 1-file, 2-syslog")
                ("log.level", boost::program_options
                        ::value<unsigned int>()->default_value(8), "log severity level")
                ("log.file", boost::program_options
                        ::value<std::string>()->default_value("fred.log"), "log file name for log.type = 1")
                ("log.syslog_facility", boost::program_options
                        ::value<int>(), "syslog facility (0..7 => LOG_LOCAL0-7), default LOG_USER")
                ("log.config_dump", boost::program_options
                        ::value<bool>()->default_value(false), "log loaded configuration data with debug severity");
        return cfg_opts;
    }
    void handle(int argc, char* argv[], FakedArgs& fa) override
    {
        boost::program_options::variables_map vm;
        handler_parse_args()(this->get_options_description(), vm, argc, argv, fa);

        log_type = vm["log.type"].as<unsigned>();
        log_level = vm["log.level"].as<unsigned>();
        log_file = vm["log.file"].as<std::string>();
        auto facility_iter = vm.find("log.syslog_facility");
        if (facility_iter != end(vm))
        {
            static const auto make_facility = [](int offset)
            {
                switch (offset)
                {
                    case 0: return LOG_LOCAL0;
                    case 1: return LOG_LOCAL1;
                    case 2: return LOG_LOCAL2;
                    case 3: return LOG_LOCAL3;
                    case 4: return LOG_LOCAL4;
                    case 5: return LOG_LOCAL5;
                    case 6: return LOG_LOCAL6;
                    case 7: return LOG_LOCAL7;
                }
                throw std::runtime_error{"facility out of range [0, 7]"};
            };
            log_syslog_facility = make_facility(facility_iter->second.as<int>());
        }
        else
        {
            static constexpr int default_facility = LOG_USER;
            log_syslog_facility = default_facility;
        }
        log_config_dump = vm["log.config_dump"].as<bool>();
    }
    unsigned log_type;
    unsigned log_level;
    std::string log_file;
    int log_syslog_facility;
    bool log_config_dump;
};

/**
 * \class HandleLoggingArgsGrp
 * \brief logging options handler with option groups
 */

class HandleLoggingArgsGrp : public HandleGrpArgs,
                             private HandleLoggingArgs
{
public:
    std::shared_ptr<boost::program_options::options_description>
    get_options_description() override
    {
        return HandleLoggingArgs::get_options_description();
    }
    std::size_t handle(int argc, char* argv[], FakedArgs& fa, std::size_t option_group_index) override
    {
        HandleLoggingArgs::handle(argc, argv, fa);
        return option_group_index;
    }

    unsigned get_log_type() const noexcept { return HandleLoggingArgs::log_type; }
    unsigned get_log_level() const noexcept { return HandleLoggingArgs::log_level; }
    unsigned get_log_syslog_facility() const noexcept { return HandleLoggingArgs::log_syslog_facility; }
    const std::string& get_log_file() const noexcept { return HandleLoggingArgs::log_file; }
    bool get_log_config_dump() const noexcept { return HandleLoggingArgs::log_config_dump; }
};

inline void setup_logging(
        unsigned log_type,
        unsigned log_level,
        std::string log_file,
        unsigned log_syslog_facility,
        bool log_config_dump)
{
    enum class LogSeverity
    {
        emerg,
        alert,
        crit,
        err,
        warning,
        notice,
        info,
        debug,
        trace
    };
    static const auto severity_to_level = [](LogSeverity severity)
    {
        switch (severity)
        {
            case LogSeverity::emerg:
                return LibLog::Level::critical;
            case LogSeverity::alert:
                return LibLog::Level::critical;
            case LogSeverity::crit:
                return LibLog::Level::critical;
            case LogSeverity::err:
                return LibLog::Level::error;
            case LogSeverity::warning:
                return LibLog::Level::warning;
            case LogSeverity::notice:
                return LibLog::Level::info;
            case LogSeverity::info:
                return LibLog::Level::info;
            case LogSeverity::debug:
                return LibLog::Level::debug;
            case LogSeverity::trace:
                return LibLog::Level::trace;
        }
        return LibLog::Level::trace;
    };
    static const auto make_console_config = [](LogSeverity severity)
    {
        LibLog::Sink::ConsoleSinkConfig config;
        config.set_level(severity_to_level(severity));
        config.set_output_stream(LibLog::Sink::ConsoleSinkConfig::OutputStream::stderr);
        config.set_color_mode(LibLog::ColorMode::never);
        return config;
    };
    static const auto make_file_config = [](LogSeverity severity, const std::string& file)
    {
        LibLog::Sink::FileSinkConfig config{file};
        config.set_level(severity_to_level(severity));
        return config;
    };
    static const auto make_syslog_config = [](LogSeverity severity, int facility)
    {
        LibLog::Sink::SyslogSinkConfig config;
        config.set_level(severity_to_level(severity));
        config.set_syslog_facility(facility);
        return config;
    };

    const auto min_severity = static_cast<LogSeverity>(log_level);

    LibLog::LogConfig log_config;
    const auto has_log_device = [&]()
    {
        switch (log_type)
        {
            case 0:
                log_config.add_sink_config(make_console_config(min_severity));
                return true;
            case 1:
                log_config.add_sink_config(make_file_config(min_severity, log_file));
                return true;
            case 2:
                log_config.add_sink_config(make_syslog_config(min_severity, log_syslog_facility));
                return true;
        }
        return false;
    }();
    if (has_log_device)
    {
        LibLog::Log::start<LibLog::ThreadMode::multi_threaded>(log_config);
        if (log_config_dump)
        {
            for (std::string config_item = AccumulatedConfig::get_instance().pop_front();
                !config_item.empty();
                config_item = AccumulatedConfig::get_instance().pop_front())
            {
                FREDLOG_DEBUG(config_item);
            }
        }
    }
}

inline void setup_logging(const HandleLoggingArgs& args)
{
    setup_logging(
            args.log_type,
            args.log_level,
            args.log_file,
            args.log_syslog_facility,
            args.log_config_dump);
}

inline void setup_logging(const HandleLoggingArgsGrp& args_grp)
{
    setup_logging(
            args_grp.get_log_type(),
            args_grp.get_log_level(),
            args_grp.get_log_file(),
            args_grp.get_log_syslog_facility(),
            args_grp.get_log_config_dump());
}

#endif//HANDLE_LOGGING_ARGS_HH_431EDB4A1D714CE3BD4948CB99DFA42C
