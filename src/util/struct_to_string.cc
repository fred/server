/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/util/struct_to_string.hh"

namespace Util {

StructToString::StructToString()
{
}

StructToString& StructToString::add(const std::string& name, const std::string& value)
{
    return _add(name, "'" + value + "'");
}

std::string StructToString::finish()
{
    if (buffer_.empty())
    {
        return "{ }";
    }
    const std::string retval = "{ " + buffer_ + " }";
    buffer_.clear();
    return retval;
}

StructToString& StructToString::_add(const std::string& name, const std::string& value)
{
    if (!buffer_.empty())
    {
        buffer_ += ", ";
    }
    buffer_ += name + ": " + value;
    return *this;
}

} // namespace Util
