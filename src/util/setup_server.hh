/*
 * Copyright (C) 2011-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SETUP_SERVER_HH_B3A8DA8BD3A64FAEBB957E50B12B46BE
#define SETUP_SERVER_HH_B3A8DA8BD3A64FAEBB957E50B12B46BE

#include "src/util/corba_wrapper.hh"
#include "src/util/cfg/config_handler_decl.hh"

void run_server(CfgArgs* cfg_instance_ptr , CorbaContainer* corba_instance_ptr);

void run_server(CfgArgGroups* cfg_instance_ptr , CorbaContainer* corba_instance_ptr);

void corba_init();

void corba_grpinit();

#endif//SETUP_SERVER_HH_B3A8DA8BD3A64FAEBB957E50B12B46BE
