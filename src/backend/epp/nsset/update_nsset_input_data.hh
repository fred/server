/*
 * Copyright (C) 2017-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef UPDATE_NSSET_INPUT_DATA_HH_B0E1C0624AD24C77A6BFA86F6A640FAC
#define UPDATE_NSSET_INPUT_DATA_HH_B0E1C0624AD24C77A6BFA86F6A640FAC

#include "src/backend/epp/nsset/dns_host_input.hh"
#include "util/optional_value.hh"

#include <boost/optional.hpp>

#include <string>
#include <vector>

namespace Epp {
namespace Nsset {

struct UpdateNssetInputData
{
    std::string handle;
    Optional<Nullable<std::string>> authinfopw;
    std::vector<DnsHostInput> dns_hosts_add;
    std::vector<DnsHostInput> dns_hosts_rem;
    std::vector<std::string> tech_contacts_add;
    std::vector<std::string> tech_contacts_rem;
    boost::optional<short> tech_check_level;
};

} // namespace Epp::Nsset
} // namespace Epp

#endif
