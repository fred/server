/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUTHINFO_NSSET_CONFIG_DATA_HH_00BE4F56222A47E88CD2D6AB341009E8
#define AUTHINFO_NSSET_CONFIG_DATA_HH_00BE4F56222A47E88CD2D6AB341009E8

namespace Epp {
namespace Nsset {

struct AuthinfoNssetConfigData
{
    bool rifd_partially_disclose_contact_emails;
};

} // namespace Fred::Backend::Epp::Nsset
} // namespace Fred::Backend::Epp

#endif
