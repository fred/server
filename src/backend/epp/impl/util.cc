/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/backend/epp/impl/util.hh"

#include "libfred/object_state/object_has_state.hh"
#include "libfred/registrable_object/contact/info_contact.hh"

namespace Epp {

bool is_contact_linkable(
        const LibFred::OperationContext& _ctx,
        const std::string& _contact_handle)
{
    const auto contact_id = LibFred::InfoContactByHandle(_contact_handle).exec(_ctx).info_contact_data.id;
    return !LibFred::ObjectHasState(contact_id, LibFred::Object_State::server_link_prohibited).exec(_ctx);
}

bool is_authinfopw_strong_enough(const std::string& authinfopw, int length_min)
{
    return authinfopw.length() >= length_min;
}

} // namespace Epp
