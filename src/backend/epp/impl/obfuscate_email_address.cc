/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/backend/epp/impl/obfuscate_email_address.hh"

#include <regex>

namespace Epp {
namespace Impl {

std::string obfuscate_email_address(const std::string& _email_address)
{
    static const auto email_regex = std::regex{"(.+)@(.+)\\..+"};
    static const auto expected_number_of_parts = 3;
    std::smatch match;
    if (!std::regex_match(_email_address, match, email_regex) || match.size() != expected_number_of_parts)
    {
        return "";
    }
    return std::string(match[1], 0, 1) + "*****@" + std::string(match[2], 0, 1) + "*****.*";
}

} // namespace Epp::Contact::Impl
} // namespace Epp
