/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUTHINFO_DOMAIN_LOCALIZED_RESPONSE_HH_F89F835811294CB599D96BFE24F9F802
#define AUTHINFO_DOMAIN_LOCALIZED_RESPONSE_HH_F89F835811294CB599D96BFE24F9F802

#include "src/backend/epp/epp_response_success_localized.hh"

#include <string>
#include <vector>

namespace Epp {
namespace Domain {

struct AuthinfoDomainLocalizedResponse
{
    EppResponseSuccessLocalized epp_response_success_localized;
    std::vector<std::string> emails;
};

} // namespace Epp::Domain
} // namespace Epp

#endif
