/*
 * Copyright (C) 2017-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef UPDATE_DOMAIN_INPUT_DATA_HH_20735D14D1A54D8293D414DDFE019687
#define UPDATE_DOMAIN_INPUT_DATA_HH_20735D14D1A54D8293D414DDFE019687

#include "src/backend/epp/domain/domain_enum_validation.hh"
#include "util/db/nullable.hh"
#include "util/optional_value.hh"

#include <boost/optional.hpp>

#include <string>

namespace Epp {
namespace Domain {

struct UpdateDomainInputData
{
    std::string fqdn;
    Optional<std::string> registrant_chg;
    Optional<Nullable<std::string>> authinfopw_chg;
    Optional<Nullable<std::string>> nsset_chg;
    Optional<Nullable<std::string>> keyset_chg;
    std::vector<std::string> admin_contacts_add;
    std::vector<std::string> admin_contacts_rem;
    std::vector<std::string> tmpcontacts_rem;
    boost::optional<::Epp::Domain::EnumValidationExtension> enum_validation;
};

} // namespace Epp::Domain
} // namespace Epp

#endif
