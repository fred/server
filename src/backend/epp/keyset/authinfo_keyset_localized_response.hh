/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AUTHINFO_KEYSET_LOCALIZED_RESPONSE_HH_B3B8B906B9204D84B2BE43AFF3334AD6
#define AUTHINFO_KEYSET_LOCALIZED_RESPONSE_HH_B3B8B906B9204D84B2BE43AFF3334AD6

#include "src/backend/epp/epp_response_success_localized.hh"

#include <string>
#include <vector>

namespace Epp {
namespace Keyset {

struct AuthinfoKeysetLocalizedResponse
{
    EppResponseSuccessLocalized epp_response_success_localized;
    std::vector<std::string> emails;
};

} // namespace Epp::Keyset
} // namespace Epp

#endif
