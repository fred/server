/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/backend/epp/contact/authinfo_contact_localized.hh"

#include "src/backend/epp/contact/authinfo_contact.hh"
#include "src/backend/epp/epp_response_failure.hh"
#include "src/backend/epp/epp_response_failure_localized.hh"
#include "src/backend/epp/epp_response_success.hh"
#include "src/backend/epp/epp_response_success_localized.hh"
#include "src/backend/epp/impl/action.hh"

#include "util/log/log.hh"

#include <boost/format.hpp>
#include <boost/format/free_funcs.hpp>

namespace Epp {
namespace Contact {

AuthinfoContactLocalizedResponse authinfo_contact_localized(
        const std::string& _contact_handle,
        const AuthinfoContactConfigData& _authinfo_contact_config_data,
        const SessionData& _session_data)
{
    try
    {
        LibFred::OperationContextCreator ctx;

        const auto emails =
                authinfo_contact(
                        ctx,
                        _contact_handle,
                        _authinfo_contact_config_data,
                        _session_data);

        const AuthinfoContactLocalizedResponse authinfo_contact_localized_response{
                EppResponseSuccessLocalized(
                        ctx,
                        EppResponseSuccess(EppResultSuccess(EppResultCode::command_completed_successfully)),
                        _session_data.lang),
                emails};

        ctx.commit_transaction();

        return authinfo_contact_localized_response;
    }
    catch (const EppResponseFailure& e)
    {
        LibFred::OperationContextCreator ctx;
        FREDLOG_INFO(std::string("authinfo_contact_localized: ") + e.what());
        throw EppResponseFailureLocalized(
                ctx,
                e,
                _session_data.lang);
    }
    catch (const std::exception& e)
    {
        LibFred::OperationContextCreator ctx;
        FREDLOG_INFO(std::string("authinfo_contact_localized failure: ") + e.what());
        throw EppResponseFailureLocalized(
                ctx,
                EppResponseFailure(EppResultFailure(EppResultCode::command_failed)),
                _session_data.lang);
    }
    catch (...)
    {
        LibFred::OperationContextCreator ctx;
        FREDLOG_INFO("unexpected exception in authinfo_contact_localized function");
        throw EppResponseFailureLocalized(
                ctx,
                EppResponseFailure(EppResultFailure(EppResultCode::command_failed)),
                _session_data.lang);
    }
}

} // namespace Epp::Contact
} // namespace Epp
