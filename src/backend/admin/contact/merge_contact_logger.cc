/*
 * Copyright (C) 2017-2024  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/backend/admin/contact/merge_contact_logger.hh"

#include "libgrill/connection.hh"
#include "libgrill/logger/close_log_entry.hh"
#include "libgrill/logger/create_log_entry.hh"
#include "libgrill/logger/log_entry_property.hh"
#include "libgrill/logger/object_reference.hh"

#include <boost/uuid/uuid_io.hpp>

#include <utility>
#include <vector>

using Property = LibGrill::Logger::LogEntryProperty;
using LogEntryPropertyValue = LibGrill::Logger::LogEntryPropertyValue;

void logger_merge_contact_transform_output_data(
        const LibFred::MergeContactOutput &_merge_data,
        LibGrill::Logger::Properties& _properties,
        LibGrill::Logger::ObjectReferences& _object_references)
{
    _object_references[LibGrill::Logger::ObjectReferenceType{"contact"}].push_back(LibGrill::Logger::ObjectReferenceValue{boost::uuids::to_string(_merge_data.contactid.src_contact_uuid)});
    _object_references[LibGrill::Logger::ObjectReferenceType{"contact"}].push_back(LibGrill::Logger::ObjectReferenceValue{boost::uuids::to_string(_merge_data.contactid.dst_contact_uuid)});
    for (std::vector<LibFred::MergeContactUpdateDomainRegistrant>::const_iterator i = _merge_data.update_domain_registrant.begin();
            i != _merge_data.update_domain_registrant.end(); ++i)
    {
        _properties[Property::Name{"command"}].push_back(
                LogEntryPropertyValue{
                        Property::Value{"update_domain"},
                        Property::Children{
                                {Property::Name{"handle"}, Property::Value{i->fqdn}},
                                {Property::Name{"registrant"}, Property::Value{i->set_registrant}}}});

        _object_references[LibGrill::Logger::ObjectReferenceType{"domain"}].push_back(LibGrill::Logger::ObjectReferenceValue{boost::uuids::to_string(i->domain_uuid)});
    }
    for (std::vector<LibFred::MergeContactUpdateDomainAdminContact>::const_iterator i = _merge_data.update_domain_admin_contact.begin();
            i != _merge_data.update_domain_admin_contact.end(); ++i)
    {
        _properties[Property::Name{"command"}].push_back(
                LogEntryPropertyValue{
                        Property::Value{"update_domain"},
                        Property::Children{
                                {Property::Name{"handle"}, Property::Value{i->fqdn}},
                                {Property::Name{"remAdmin"}, Property::Value{i->rem_admin_contact}},
                                {Property::Name{"addAdmin"}, Property::Value{i->add_admin_contact}}}});

        _object_references[LibGrill::Logger::ObjectReferenceType{"domain"}].push_back(LibGrill::Logger::ObjectReferenceValue{boost::uuids::to_string(i->domain_uuid)});
    }
    for (std::vector<LibFred::MergeContactUpdateNssetTechContact>::const_iterator i = _merge_data.update_nsset_tech_contact.begin();
            i != _merge_data.update_nsset_tech_contact.end(); ++i)
    {
        _properties[Property::Name{"command"}].push_back(
                LogEntryPropertyValue{
                        Property::Value{"update_nsset"},
                        Property::Children{
                                {Property::Name{"handle"}, Property::Value{i->handle}},
                                {Property::Name{"remTech"}, Property::Value{i->rem_tech_contact}},
                                {Property::Name{"addTech"}, Property::Value{i->add_tech_contact}}}});

        _object_references[LibGrill::Logger::ObjectReferenceType{"nsset"}].push_back(LibGrill::Logger::ObjectReferenceValue{boost::uuids::to_string(i->nsset_uuid)});
    }
    for (std::vector<LibFred::MergeContactUpdateKeysetTechContact>::const_iterator i = _merge_data.update_keyset_tech_contact.begin();
            i != _merge_data.update_keyset_tech_contact.end(); ++i)
    {
        _properties[Property::Name{"command"}].push_back(
                LogEntryPropertyValue{
                        Property::Value{"update_keyset"},
                        Property::Children{
                                {Property::Name{"handle"}, Property::Value{i->handle}},
                                {Property::Name{"remTech"}, Property::Value{i->rem_tech_contact}},
                                {Property::Name{"addTech"}, Property::Value{i->add_tech_contact}}}});

        _object_references[LibGrill::Logger::ObjectReferenceType{"keyset"}].push_back(LibGrill::Logger::ObjectReferenceValue{boost::uuids::to_string(i->keyset_uuid)});
    }
}


LibGrill::Logger::LogEntryId logger_merge_contact_create_request(
        const LoggerArgs& _logger_args,
        const std::string &_src_contact,
        const std::string &_dst_contact)
{
    LibGrill::Connection<LibGrill::Service::Logger> logger_connection{LibGrill::Connection<LibGrill::Service::Logger>::ConnectionString{_logger_args.endpoint}};
    LibGrill::Logger::CreateLogEntryRequest request(
            LibGrill::Logger::LogEntryType{"ContactMerge"},
            LibGrill::Logger::LogEntryService{"Admin"});
    LibGrill::Logger::Properties properties{
            {Property::Name{"src_contact"},
                    {LogEntryPropertyValue{
                            Property::Value{_src_contact},
                            Property::Children{}}}},
            {Property::Name{"dst_contact"},
                    {LogEntryPropertyValue{
                            Property::Value{_dst_contact},
                            Property::Children{}}}}};
    request.add(std::move(properties));

    try
    {
        const auto log_entry_id = LibGrill::Logger::create_log_entry(logger_connection, request);
        return log_entry_id;
    }
    catch (const LibGrill::Logger::CreateLogEntryException& e)
    {
        throw std::runtime_error(std::string("unable to log merge contact request: ") + e.error_message());
    }
}


void logger_merge_contact_close(
        const LoggerArgs& _logger_args,
        const LibGrill::Logger::LogEntryId& _req_id,
        LibGrill::Logger::Properties _properties,
        LibGrill::Logger::ObjectReferences _object_references,
        const LibGrill::Logger::OperationResultName& _result)
{
    LibGrill::Connection<LibGrill::Service::Logger> logger_connection{LibGrill::Connection<LibGrill::Service::Logger>::ConnectionString{_logger_args.endpoint}};
    LibGrill::Logger::CloseLogEntryRequest request(
            _req_id,
            LibGrill::Logger::OperationResultName{_result});
    request.add(std::move(_properties));
    request.add(std::move(_object_references));
    LibGrill::Logger::Properties properties{
                {Property::Name{"opTRID"},
                        {LogEntryPropertyValue{
                                Property::Value{Util::make_svtrid(*(_req_id.get_numerical_id()))},
                                Property::Children{}}}}};
    request.add(std::move(properties));

    try
    {
        LibGrill::Logger::close_log_entry(logger_connection, request);
    }
    catch (const LibGrill::Logger::CloseLogEntryException& e)
    {
        throw std::runtime_error(std::string("unable to log merge contact request: ") + e.error_message());
    }
}


void logger_merge_contact_close_request_success(
        const LoggerArgs& _logger_args,
        const LibGrill::Logger::LogEntryId& _req_id,
        const LibFred::MergeContactOutput& _merge_data)
{
    LibGrill::Logger::Properties properties;
    LibGrill::Logger::ObjectReferences object_references;
    logger_merge_contact_transform_output_data(_merge_data, properties, object_references),
    logger_merge_contact_close(
            _logger_args,
            _req_id,
            std::move(properties),
            std::move(object_references),
            LibGrill::Logger::OperationResultName{"Success"});
}


void logger_merge_contact_close_request_fail(
        const LoggerArgs& _logger_args,
        const LibGrill::Logger::LogEntryId& _req_id)
{
    LibGrill::Logger::Properties properties;
    LibGrill::Logger::ObjectReferences object_references;
    logger_merge_contact_close(
            _logger_args,
            _req_id,
            std::move(properties),
            std::move(object_references),
            LibGrill::Logger::OperationResultName{"Fail"});
}

