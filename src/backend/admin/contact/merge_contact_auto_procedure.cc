/*
 * Copyright (C) 2013-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/backend/admin/contact/merge_contact_auto_procedure.hh"
#include "src/backend/admin/contact/merge_contact.hh"
#include "src/backend/admin/contact/merge_contact_reporting.hh"

#include "util/util.hh"

#include "libfred/registrable_object/contact/find_contact_duplicates.hh"
#include "libfred/registrable_object/contact/info_contact.hh"
#include "libfred/registrable_object/contact/merge_contact.hh"
#include "libfred/registrable_object/contact/merge_contact_email_notification_data.hh"
#include "libfred/registrable_object/contact/merge_contact_selection.hh"

#include "libhermes/libhermes.hh"

#include <boost/algorithm/string/join.hpp>
#include <boost/assign/list_of.hpp>
#include <boost/uuid/uuid_io.hpp>

#include <algorithm>
#include <sstream>

namespace Admin {

namespace {

std::set<std::string> set_diff(const std::set<std::string>& _set, const std::set<std::string>& _subset) {
    std::set<std::string> result;
    std::set_difference(
        _set.begin(), _set.end(),
        _subset.begin(), _subset.end(),
        std::insert_iterator<std::set<std::string> >(result, result.begin())
    );
    return result;
}

boost::optional<std::string> get_system_registrar_handle(const LibFred::OperationContext& ctx) {
    const Database::Result db_result = ctx.get_conn().exec(
        "SELECT handle FROM registrar WHERE system is True"
    );
    return db_result.size() > 0
        ?
        static_cast<std::string>(db_result[0][0])
        :
        boost::optional<std::string>();
}

bool registrar_exists(const LibFred::OperationContext& ctx, std::string _registrar_handle) {
    const Database::Result db_result = ctx.get_conn().exec_params(
        "SELECT id FROM registrar WHERE handle = UPPER($1::text)"
        , Database::query_param_list(_registrar_handle)
    );
    return db_result.size() != 0;
}

} // namespace Admin::{anonymous}

std::vector<LibFred::MergeContactNotificationEmailWithAddr> email_notification(
    const MessengerArgs& _messenger_args,
    const std::vector<LibFred::MergeContactEmailNotificationInput>& email_notification_input_vector)
{
    auto notification_emails =
            [&](){
                LibFred::OperationContextCreator enctx;
                auto merge_contact_notification_emails_with_addr =
                        LibFred::MergeContactNotificationEmailAddr(LibFred::MergeContactEmailNotificationData(email_notification_input_vector)
                                                                           .exec(enctx)).exec(enctx);
                enctx.commit_transaction();
                return merge_contact_notification_emails_with_addr;
            }();

    for (const auto& notification_email : notification_emails)
    {
        std::vector<LibHermes::StructValue> domain_registrant_list;
        std::vector<LibHermes::StructValue> domain_admin_list;
        std::vector<LibHermes::StructValue> nsset_tech_list;
        std::vector<LibHermes::StructValue> keyset_tech_list;
        std::vector<LibHermes::StructValue> removed_list;
        std::set<LibHermes::Reference> references;
       
        domain_registrant_list.reserve(notification_email.email_data.domain_registrant_list.size());
        for (const auto& domain_registrant : notification_email.email_data.domain_registrant_list)
        {
            domain_registrant_list.push_back(LibHermes::StructValue{domain_registrant});
        }

        domain_admin_list.reserve(notification_email.email_data.domain_admin_list.size());
        for (const auto& domain_admin : notification_email.email_data.domain_admin_list)
        {
            domain_admin_list.push_back(LibHermes::StructValue{domain_admin});
        }

        nsset_tech_list.reserve(notification_email.email_data.nsset_tech_list.size());
        for (const auto& nsset_tech : notification_email.email_data.nsset_tech_list)
        {
            nsset_tech_list.push_back(LibHermes::StructValue{nsset_tech});
        }

        keyset_tech_list.reserve(notification_email.email_data.keyset_tech_list.size());
        for (const auto& keyset_tech : notification_email.email_data.keyset_tech_list)
        {
            keyset_tech_list.push_back(LibHermes::StructValue{keyset_tech});
        }

        removed_list.reserve(notification_email.email_data.removed_list.size());
        for (const auto& removed : notification_email.email_data.removed_list)
        {
            removed_list.push_back(LibHermes::StructValue{removed});
        }

        for (const auto& updated_domain : notification_email.email_data.updated_domains)
        {
            references.insert(LibHermes::Reference{LibHermes::Reference::Type{"domain"}, LibHermes::Reference::Value{boost::uuids::to_string(updated_domain)}});
        }

        for (const auto& updated_nsset : notification_email.email_data.updated_nssets)
        {
            references.insert(LibHermes::Reference{LibHermes::Reference::Type{"nsset"}, LibHermes::Reference::Value{boost::uuids::to_string(updated_nsset)}});
        }

        for (const auto& updated_keyset : notification_email.email_data.updated_keysets)
        {
            references.insert(LibHermes::Reference{LibHermes::Reference::Type{"keyset"}, LibHermes::Reference::Value{boost::uuids::to_string(updated_keyset)}});
        }

        for (const auto& removed_contact : notification_email.email_data.removed_contacts)
        {
            references.insert(LibHermes::Reference{LibHermes::Reference::Type{"contact"}, LibHermes::Reference::Value{boost::uuids::to_string(removed_contact)}});
        }

        LibHermes::Struct email_template_params{
            {LibHermes::StructKey{"dst_contact_handle"}, LibHermes::StructValue{notification_email.email_data.dst_contact_handle}},
            {LibHermes::StructKey{"domain_registrant_list"}, LibHermes::StructValue{domain_registrant_list}},
            {LibHermes::StructKey{"domain_admin_list"}, LibHermes::StructValue{domain_admin_list}},
            {LibHermes::StructKey{"nsset_tech_list"}, LibHermes::StructValue{nsset_tech_list}},
            {LibHermes::StructKey{"keyset_tech_list"}, LibHermes::StructValue{keyset_tech_list}},
            {LibHermes::StructKey{"removed_list"}, LibHermes::StructValue{removed_list}}};

        try 
        {
            auto email =
                    LibHermes::Email::make_minimal_email(
                            {{LibHermes::Email::RecipientEmail{notification_email.notification_email_addr},
                             {LibHermes::Email::RecipientUuid{notification_email.email_data.dst_contact_uuid}}}},
                            LibHermes::Email::SubjectTemplate{"merge-contacts-auto-subject.txt"},
                            LibHermes::Email::BodyTemplate{"merge-contacts-auto-body.txt"});
            email.type = LibHermes::Email::Type{"merge_contacts_auto"};
            email.context = email_template_params;

            FREDLOG_DEBUG("send_invoices: sending email");

            LibHermes::Connection<LibHermes::Service::EmailMessenger> connection{
                    LibHermes::Connection<LibHermes::Service::EmailMessenger>::ConnectionString{
                            _messenger_args.endpoint}};

            LibHermes::Email::send(
                    connection,
                    email,
                    LibHermes::Email::Archive{_messenger_args.archive},
                    references);
        }
        catch (const LibHermes::Email::SendFailed& e)
        {
            FREDLOG_WARNING(boost::str(boost::format("gRPC exception caught while sending email: gRPC error code: %1% error message: %2% grpc_message_json: %3%") %
                        e.error_code() % e.error_message() % e.grpc_message_json()));
            throw std::runtime_error("failed to send email to recipient");
        }
        catch (const std::exception& e)
        {
            FREDLOG_WARNING(boost::str(boost::format("std::exception caught while sending email: %1%") % e.what()));
            throw std::runtime_error("failed to send email to recipient");
        }
        catch (...)
        {
            FREDLOG_WARNING("exception caught while sending email");
            throw std::runtime_error("failed to send email to recipient");
        }
    }
    return notification_emails;
}

MergeContactAutoProcedure::MergeContactAutoProcedure(
    const MessengerArgs& _messenger_args,
    const LoggerArgs& _logger_args,
    const std::string& _registrar)
    : messenger_args_(_messenger_args)
    , logger_args_(_logger_args)
    , registrar_(_registrar)
{ }

MergeContactAutoProcedure::MergeContactAutoProcedure(
    const MessengerArgs& _messenger_args,
    const LoggerArgs& _logger_args,
    const std::string& _registrar,
    const Optional<bool>& _dry_run,
    const Optional<unsigned short>& _verbose)
    : messenger_args_(_messenger_args),
      logger_args_(_logger_args),
      registrar_(_registrar),
      dry_run_(_dry_run),
      verbose_(_verbose)
{ }

MergeContactAutoProcedure& MergeContactAutoProcedure::set_dry_run(
    const Optional<bool>& _dry_run
)
{
    dry_run_ = _dry_run;
    return *this;
}

MergeContactAutoProcedure& MergeContactAutoProcedure::set_selection_filter_order(
    const std::vector<std::string>& _selection_filter_order)
{
    /* XXX: this throws - should do better error reporting */
    Util::FactoryHaveSupersetOfKeys::require(
            LibFred::get_default_contact_selection_filter_factory(),
            _selection_filter_order);
    selection_filter_order_ = _selection_filter_order;
    return *this;
}


MergeContactAutoProcedure& MergeContactAutoProcedure::set_verbose(
    const Optional<unsigned short>& _verbose)
{
    verbose_ = _verbose;
    return *this;
}


bool MergeContactAutoProcedure::is_set_dry_run() const
{
    return (dry_run_.isset() && dry_run_.get_value() == true);
}


std::vector<std::string> MergeContactAutoProcedure::get_default_selection_filter_order() const
{
    std::vector<std::string> tmp = boost::assign::list_of
        (LibFred::MCS_FILTER_IDENTIFIED_CONTACT)
        (LibFred::MCS_FILTER_IDENTITY_ATTACHED)
        (LibFred::MCS_FILTER_CONDITIONALLY_IDENTIFIED_CONTACT)
        (LibFred::MCS_FILTER_HANDLE_MOJEID_SYNTAX)
        (LibFred::MCS_FILTER_MAX_DOMAINS_BOUND)
        (LibFred::MCS_FILTER_MAX_OBJECTS_BOUND)
        (LibFred::MCS_FILTER_RECENTLY_UPDATED)
        (LibFred::MCS_FILTER_NOT_REGCZNIC)
        (LibFred::MCS_FILTER_RECENTLY_CREATED);
    return tmp;
}


unsigned short MergeContactAutoProcedure::get_verbose_level() const
{
    if (verbose_.isset()) {
        return verbose_.get_value();
    }
    if (is_set_dry_run()) {
        return 3;
    }
    return 0;
}

LibFred::MergeContactOutput MergeContactAutoProcedure::merge_contact(
    const std::string& _src_contact,
    const std::string& _dst_contact,
    const std::string& _system_registrar
)
{
    Admin::MergeContact merge_op = Admin::MergeContact(_src_contact, _dst_contact, _system_registrar);

    if (is_set_dry_run()) {
        dry_run_info_.add_search_excluded(_src_contact);
        dry_run_info_.add_search_excluded(_dst_contact);
        return merge_op.exec_dry_run();
    }

    return merge_op.exec(logger_args_);
}

std::vector<LibFred::MergeContactNotificationEmailWithAddr> MergeContactAutoProcedure::exec(std::ostream& _output_stream)
{
    LibFred::OperationContextCreator ctx;

    std::vector<LibFred::MergeContactNotificationEmailWithAddr> ret_email_notifications;

    const boost::optional<std::string> system_registrar = get_system_registrar_handle(ctx);
    if (!system_registrar) {
        throw std::runtime_error("no system registrar found");
    }

    if (!registrar_exists(ctx, registrar_)) {
        throw std::runtime_error(std::string("registrar: '") + registrar_ + "' not found");
    }

    const std::vector<std::string> selection_filter =
        !selection_filter_order_.empty()
        ? selection_filter_order_
        : get_default_selection_filter_order();

    OutputIndenter indenter(2, 0, ' ');

    if (get_verbose_level() > 0) {
        _output_stream << format_header(boost::str(boost::format("REGISTRAR: %1%") % registrar_), indenter);
    }

    std::set<std::string> duplicate_contacts;
    std::set<std::string> invalid_contacts;

    while (true) {

        duplicate_contacts = LibFred::Contact::FindContactDuplicates()
            .set_registrar(Optional<std::string>(registrar_))
            .set_exclude_contacts(invalid_contacts)
            .exec(ctx);

        if(is_set_dry_run()) {
            duplicate_contacts = set_diff(duplicate_contacts, dry_run_info_.any_search_excluded);
        }

        if (duplicate_contacts.size() < 2) break;

        summary_info_.inc_merge_set();
        MergeContactOperationSummary merge_set_operation_info;
        std::vector<LibFred::MergeContactEmailNotificationInput> email_notification_input_vector;

        boost::optional<LibFred::MergeContactSelectionOutput> dst_contact;

        while (duplicate_contacts.size()) {

            if (!dst_contact) {
                dst_contact = LibFred::MergeContactSelection(
                                  std::vector<std::string>(duplicate_contacts.begin(), duplicate_contacts.end()),
                                  selection_filter
                              ).exec(ctx);

                if(!dst_contact) {
                    throw std::runtime_error("dst_contact not identified\n");
                }

                FREDLOG_DEBUG(boost::format("contact duplicates set: { %1% }") % boost::algorithm::join(duplicate_contacts, ", "));

                FREDLOG_DEBUG(boost::format("winner handle: %1%") % (*dst_contact).handle);

                duplicate_contacts.erase((*dst_contact).handle);

            }

            if(duplicate_contacts.size() < 1) break;

            const std::string src_contact = *(duplicate_contacts.begin());

            try {
                MergeContactOperationSummary merge_operation_info;

                summary_info_.inc_merge_operation();

                const LibFred::MergeContactOutput merge_data =
                    merge_contact(src_contact, (*dst_contact).handle, *system_registrar);

                if(!is_set_dry_run()) {
                    email_notification_input_vector.push_back(
                        LibFred::MergeContactEmailNotificationInput(src_contact, (*dst_contact).handle, merge_data)
                    );
                }

                if (get_verbose_level() > 0) {
                    merge_operation_info.add_merge_output(merge_data);
                    merge_set_operation_info.add_merge_output(merge_data);
                    all_merge_operation_info_.add_merge_output(merge_data);
                }

                if (get_verbose_level() > 2) {
                    _output_stream << format_merge_contact_output(merge_data, src_contact, (*dst_contact).handle, summary_info_, indenter);
                    _output_stream << merge_operation_info.format(indenter.dive());
                }
            }
            catch(const LibFred::MergeContact::Exception& ex)
            {
                bool exception_handled = false;

                if (ex.is_set_src_contact_invalid()) {
                    invalid_contacts.insert(ex.get_src_contact_invalid());
                    exception_handled = true;
                    summary_info_.inc_invalid_contacts();
                }

                if (ex.is_set_object_blocked()) { // object linked to src contact blocked, skip src contact
                    invalid_contacts.insert(src_contact);
                    exception_handled = true;
                    summary_info_.inc_linked_contacts();
                }

                if (ex.is_set_dst_contact_invalid()) {
                    invalid_contacts.insert(ex.get_dst_contact_invalid());
                    dst_contact = boost::optional<LibFred::MergeContactSelectionOutput>(); // invalidate dst_contact
                    exception_handled = true;
                    summary_info_.inc_invalid_contacts();
                }

                if (!exception_handled) {
                    throw;
                }
            }

            FREDLOG_DEBUG(boost::format("skip invalid contacts set: { %1% }") % boost::algorithm::join(invalid_contacts, ", "));

            duplicate_contacts = set_diff(duplicate_contacts, invalid_contacts);

            duplicate_contacts.erase(src_contact); // remove if not already removed as invalid

        }

        if (get_verbose_level() > 1) {
            _output_stream << format_header(
                boost::str(boost::format("[-/%1%/-] MERGE SET SUMMARY") % summary_info_.merge_sets_total),
                indenter);
            _output_stream << merge_set_operation_info.format(indenter);
        }

        if (!is_set_dry_run()) {
            ret_email_notifications = email_notification(messenger_args_, email_notification_input_vector);
        }

    }

    if (get_verbose_level() > 0) {
        _output_stream << format_header(
            boost::str(boost::format("[%1%/%2%/-] PROCEDURE RUN SUMMARY") % summary_info_.merge_operations_total % summary_info_.merge_sets_total),
            indenter);
        _output_stream << all_merge_operation_info_.format(indenter);
    }
    if (!is_set_dry_run()) {
        ctx.commit_transaction();
    }
    return ret_email_notifications;
}


}

