/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/backend/admin/registrar/create_registrar.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrar/create_registrar.hh"
#include "util/log/log.hh"

namespace Admin {
namespace Registrar {

const char* CreateRegistrarException::what() const noexcept
{
    return "Failed to create registrar due to an unknown exception.";
}

const char* RegistrarAlreadyExists::what() const noexcept
{
    return "Registrar already exists in database.";
}

const char* VariableSymbolAlreadyExists::what() const noexcept
{
    return "Variable symbol already exists in database.";
}

} // namespace Admin::Registrar
} // namespace Admin

using namespace Admin::Registrar;

unsigned long long Admin::Registrar::create_registrar(
        const std::string& _handle,
        const std::string& _name,
        const std::string& _organization,
        const std::vector<std::string>& _street,
        const std::string& _city,
        const boost::optional<std::string>& _state_or_province,
        const boost::optional<std::string>& _postal_code,
        const std::string& _country,
        const boost::optional<std::string>& _telephone,
        const boost::optional<std::string>& _fax,
        const std::string& _email,
        const std::string& _url,
        bool _system,
        const boost::optional<std::string>& _ico,
        const std::string& _dic,
        const boost::optional<std::string>& _variable_symbol,
        const boost::optional<std::string>& _payment_memo_regex,
        boost::optional<bool> _vat_payer,
        bool _internal)
{
    FREDLOG_SET_CONTEXT(LogContext, log_ctx, __func__);
    FREDLOG_DEBUG("Registrar handle: " + _handle);
    const std::string operation_name = "LibFred::CreateRegistrar() ";

    LibFred::OperationContextCreator ctx;

    FREDLOG_TRACE("[CALL] " + operation_name);
    LibFred::CreateRegistrar registrar{
            _handle,
            _name,
            _organization,
            _street,
            _city,
            _country,
            _email,
            _url,
            _dic,
            _system,
            _internal};
    if (_state_or_province != boost::none)
    {
        registrar.set_stateorprovince(*_state_or_province);
    }
    if (_postal_code != boost::none)
    {
        registrar.set_postalcode(*_postal_code);
    }
    if (_telephone != boost::none)
    {
        registrar.set_telephone(*_telephone);
    }
    if (_fax != boost::none)
    {
        registrar.set_fax(*_fax);
    }
    if (_ico != boost::none)
    {
        registrar.set_ico(*_ico);
    }
    if (_variable_symbol != boost::none)
    {
        registrar.set_variable_symbol(*_variable_symbol);
    }
    if (_payment_memo_regex != boost::none)
    {
        registrar.set_payment_memo_regex(*_payment_memo_regex);
    }
    if (_vat_payer != boost::none)
    {
        registrar.set_vat_payer(*_vat_payer);
    }

    try
    {
        const unsigned long long id = registrar.exec(ctx);
        ctx.commit_transaction();
        return id;
    }
    catch (const ::LibFred::CreateRegistrar::Exception& e)
    {
        if (e.is_set_invalid_registrar_handle())
        {
            FREDLOG_WARNING(operation_name + e.what());
            throw RegistrarAlreadyExists();
        }
        if (e.is_set_invalid_registrar_varsymb())
        {
            FREDLOG_WARNING(operation_name + e.what());
            throw VariableSymbolAlreadyExists();
        }
        FREDLOG_ERROR(operation_name + e.what());
        throw CreateRegistrarException();
    }
    catch (const std::exception& e)
    {
        FREDLOG_ERROR(operation_name + e.what());
        throw CreateRegistrarException();
    }
    throw CreateRegistrarException();
}
