/*
 * Copyright (C) 2018-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/backend/admin/registrar/update_registrar.hh"
#include "libfred/opcontext.hh"
#include "libfred/registrar/exceptions.hh"
#include "libfred/registrar/update_registrar.hh"
#include "util/log/log.hh"

namespace Admin {
namespace Registrar {

const char* UpdateRegistrarException::what() const noexcept
{
    return "Failed to update registrar due to an unknown exception.";
}

const char* UpdateRegistrarNonexistent::what() const noexcept
{
    return "Failed to update registrar because the registrar doesn't exist.";
}

const char* UpdateRegistrarInvalidVarSymb::what() const noexcept
{
    return "Failed to update registrar because the variable symbol already exists.";
}

const char* UpdateRegistrarInvalidHandle::what() const noexcept
{
    return "Failed to update registrar because the handle already exists.";
}

const char* UpdateRegistrarNoUpdateData::what() const noexcept
{
    return "No data for update registrar.";
}

const char* UpdateRegistrarInvalidCountryCode::what() const noexcept
{
    return "Failed to update registrar due to invalid country code.";
}

namespace {

boost::optional<std::vector<std::string>> make_street(
        const boost::optional<std::string>& street1,
        const boost::optional<std::string>& street2,
        const boost::optional<std::string>& street3)
{
    if (street1 == boost::none)
    {
        return boost::none;
    }
    if (street2 == boost::none)
    {
        return std::vector<std::string>{*street1};
    }
    if (street3 == boost::none)
    {
        return std::vector<std::string>{*street1, *street2};
    }
    return std::vector<std::string>{*street1, *street2, *street3};
}

} // namespace Admin::Registrar::{anonymous}

} // namespace Admin::Registrar
} // namespace Admin

using namespace Admin::Registrar;

void Admin::Registrar::update_registrar(unsigned long long _id,
                const boost::optional<std::string>& _handle,
                const boost::optional<std::string>& _name,
                const boost::optional<std::string>& _organization,
                const boost::optional<std::string>& _street1,
                const boost::optional<std::string>& _street2,
                const boost::optional<std::string>& _street3,
                const boost::optional<std::string>& _city,
                const boost::optional<std::string>& _state_or_province,
                const boost::optional<std::string>& _postal_code,
                const boost::optional<std::string>& _country,
                const boost::optional<std::string>& _telephone,
                const boost::optional<std::string>& _fax,
                const boost::optional<std::string>& _email,
                const boost::optional<std::string>& _url,
                boost::optional<bool> _system,
                const boost::optional<std::string>& _ico,
                const boost::optional<std::string>& _dic,
                const boost::optional<std::string>& _variable_symbol,
                const boost::optional<std::string>& _payment_memo_regex,
                boost::optional<bool> _vat_payer)
{
    FREDLOG_SET_CONTEXT(LogContext, log_ctx, __func__);
    FREDLOG_DEBUG("Registrar id: " + _id);
    const std::string operation_name = "LibFred::Registrar::UpdateRegistrarById() ";

    LibFred::OperationContextCreator ctx;

    try
    {
        FREDLOG_TRACE("[CALL] " + operation_name);
        LibFred::Registrar::UpdateRegistrarById(_id)
            .set_handle(_handle)
            .set_name(_name)
            .set_organization(_organization)
            .set_street(make_street(_street1, _street2, _street3))
            .set_city(_city)
            .set_state_or_province(_state_or_province)
            .set_postal_code(_postal_code)
            .set_country(_country)
            .set_telephone(_telephone)
            .set_fax(_fax)
            .set_email(_email)
            .set_url(_url)
            .set_system(_system)
            .set_ico(_ico)
            .set_dic(_dic)
            .set_variable_symbol(_variable_symbol)
            .set_payment_memo_regex(_payment_memo_regex)
            .set_vat_payer(_vat_payer)
            .exec(ctx);
        ctx.commit_transaction();
    }
    catch (const LibFred::Registrar::NoUpdateData& e)
    {
        FREDLOG_INFO(operation_name + e.what());
        throw UpdateRegistrarNoUpdateData();
    }
    catch (const LibFred::Registrar::NonExistentRegistrar& e)
    {
        FREDLOG_WARNING(operation_name + e.what());
        throw UpdateRegistrarNonexistent();
    }
    catch (const LibFred::Registrar::RegistrarHandleAlreadyExists& e)
    {
        FREDLOG_WARNING(operation_name + e.what());
        throw UpdateRegistrarInvalidHandle();
    }
    catch (const LibFred::Registrar::VariableSymbolAlreadyExists& e)
    {
        FREDLOG_WARNING(operation_name + e.what());
        throw UpdateRegistrarInvalidVarSymb();
    }
    catch (const LibFred::Registrar::UnknownCountryCode& e)
    {
        FREDLOG_WARNING(operation_name + e.what());
        throw UpdateRegistrarInvalidCountryCode();
    }
    catch (const std::exception& e)
    {
        FREDLOG_ERROR(operation_name + e.what());
        throw UpdateRegistrarException();
    }
}
