/*
 * Copyright (C) 2012-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
/**
 *  @contact_verification_password.h
 *  header of passwords implementation
 */

#ifndef CONTACT_VERIFICATION_PASSWORD_HH_C663C8EF11554AC2B587074DA95FFCAD
#define CONTACT_VERIFICATION_PASSWORD_HH_C663C8EF11554AC2B587074DA95FFCAD

#include <map>
#include <string>

#include "src/deprecated/libfred/public_request/public_request_impl.hh"

namespace LibFred {
namespace PublicRequest {

class ContactVerificationPassword
{
public:
    typedef std::map<std::string, std::string> MessageData;
    size_t get_password_chunk_length()const;
    ContactVerificationPassword(PublicRequestAuthImpl* _prai_ptr);
    std::string generateRandomPassword(const size_t _length);
    std::string generateRandomPassword();
    std::string generateAuthInfoPassword();
private:
    PublicRequestAuthImpl* prai_ptr_;
};//class ContactVerificationPassword

}//PublicRequest
}//Fred


#endif
