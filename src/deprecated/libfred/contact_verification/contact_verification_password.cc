/*
 * Copyright (C) 2012-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *  @contact_verification_password.cc
 *  passwords implementation
 */

#include <stdexcept>

#include "src/deprecated/libfred/contact_verification/contact_verification_password.hh"
#include "libfred/db_settings.hh"
#include "util/log/log.hh"
#include "util/map_at.hh"
#include "util/random/random.hh"
#include "util/util.hh"
#include "src/util/xmlgen.hh"

namespace LibFred {
namespace PublicRequest {

size_t ContactVerificationPassword::get_password_chunk_length()const
{
    static const size_t PASSWORD_CHUNK_LENGTH = 8;
    return PASSWORD_CHUNK_LENGTH;
}

ContactVerificationPassword::ContactVerificationPassword(PublicRequestAuthImpl* _prai_ptr)
: prai_ptr_(_prai_ptr)
{}

std::string ContactVerificationPassword::generateRandomPassword(const size_t _length)
{
    return Random::Generator().get_seq(
            "ABCDEFGHJKLMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789",
            _length);
}

std::string ContactVerificationPassword::generateRandomPassword()
{
    return generateRandomPassword(get_password_chunk_length());
}

std::string ContactVerificationPassword::generateAuthInfoPassword()
{
    unsigned long long contact_id = prai_ptr_->getObject(0).id;

    Database::Connection conn = Database::Manager::acquire();
    Database::Result rauthinfo = conn.exec_params(
            "SELECT substr(replace(o.authinfopw, ' ', ''), 1, $1::integer) "
            " FROM object o JOIN contact c ON c.id = o.id"
            " WHERE c.id = $2::integer",
            Database::query_param_list(get_password_chunk_length())
                                      (contact_id));
    if (rauthinfo.size() != 1)
    {
        throw std::runtime_error(str(boost::format(
                    "cannot retrieve authinfo for contact id=%1%")
                    % contact_id));
    }
    std::string passwd;
    /* pin1 */
    if (rauthinfo[0][0].isnull())\
    {
        passwd = generateRandomPassword(get_password_chunk_length());
    }
    else
    {
        passwd = static_cast<std::string>(rauthinfo[0][0]);
        FREDLOG_DEBUG(boost::format("authinfo w/o spaces='%s'")
            % passwd);
        /* fill with random to PASSWORD_CHUNK_LENGTH size */
        size_t to_fill = 0;
        if ((to_fill = (get_password_chunk_length() - passwd.length())
                ) > 0)
        {
            passwd += generateRandomPassword(to_fill);
            FREDLOG_DEBUG(boost::format("authinfo filled='%s'")
                % passwd);
        }
    }
    return passwd;
}

}}


