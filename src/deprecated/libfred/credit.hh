/*
 * Copyright (C) 2011-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CREDIT_HH_51F9B935380D4D2C861E4C47558CD8EC
#define CREDIT_HH_51F9B935380D4D2C861E4C47558CD8EC

#include "src/util/types/money.hh"

#include "libfred/db_settings.hh"
#include "util/decimal/decimal.hh"
#include "util/log/log.hh"

namespace LibFred {
namespace Credit {

unsigned long long add_credit_to_invoice(
        unsigned long long registrar_id,
        unsigned long long zone_id,
        Money price,
        unsigned long long invoice_id);

}//namespace LibFred::Credit
}//namespace LibFred

#endif//CREDIT_HH_51F9B935380D4D2C861E4C47558CD8EC
