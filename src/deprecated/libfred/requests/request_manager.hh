/*
 * Copyright (C) 2011-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REQUEST_MANAGER_HH_F4CF50B792BC4330B21A6EB0D5A4546F
#define REQUEST_MANAGER_HH_F4CF50B792BC4330B21A6EB0D5A4546F

namespace LibFred {
namespace Logger {

typedef std::map<std::string, unsigned long long>  RequestCountInfo;

}//namespace LibFred::Logger
}//namespace LibFred

#endif /* REQUEST_MANAGER_H_ */
