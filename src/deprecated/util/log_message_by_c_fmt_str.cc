/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/deprecated/util/log_message_by_c_fmt_str.hh"

#include <stdarg.h>
#include <stdio.h>

using namespace Fred::Deprecated::Util;

std::string Fred::Deprecated::Util::log_message_by_c_fmt_str(const char* c_fmt_str, ...)
{
    char msg_buffer[0x4000];//16KB
    msg_buffer[0] = '\0';
    va_list args;
    va_start(args, c_fmt_str);
    static constexpr int buffer_capacity = sizeof(msg_buffer) - 1;
    const auto bytes = vsnprintf(msg_buffer, buffer_capacity, c_fmt_str, args);
    va_end(args);
    const bool output_was_truncated = buffer_capacity <= bytes;
    if (output_was_truncated)
    {
        msg_buffer[buffer_capacity] = '\0';
    }
    return msg_buffer;
}
