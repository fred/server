/*
 * Copyright (C) 2006  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef LOG_MESSAGE_BY_C_FMT_HH_35AF77A705CD4C51A25DBE915252CEF2
#define LOG_MESSAGE_BY_C_FMT_HH_35AF77A705CD4C51A25DBE915252CEF2

#include <string>
  
namespace Fred {
namespace Deprecated {
namespace Util {
  
std::string log_message_by_c_fmt_str(const char* c_fmt_str, ...) __attribute__ ((__format__ (__printf__, 1, 0)));
  
}//namespace Fred::Deprecated::Util
}//namespace Fred::Deprecated
}//namespace Fred

#endif//LOG_MESSAGE_BY_C_FMT_HH_35AF77A705CD4C51A25DBE915252CEF2
