/*
 * Copyright (C) 2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef AKMD_PARAMS_HH_74F08055CE3D4AFFBE28416E96D2A08C
#define AKMD_PARAMS_HH_74F08055CE3D4AFFBE28416E96D2A08C

#include <set>
#include <string>

struct AkmdArgs
{
    std::string automatically_managed_keyset_prefix;
    std::string automatically_managed_keyset_registrar;
    std::string automatically_managed_keyset_tech_contact;
    std::set<std::string> automatically_managed_keyset_zones;
    bool disable_notifier = false;
    bool enable_request_logger = false;
};

#endif
