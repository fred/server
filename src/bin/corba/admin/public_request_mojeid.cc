/*
 * Copyright (C) 2016-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "src/bin/corba/admin/public_request_mojeid.hh"

#include "src/backend/mojeid/messenger_configuration.hh"
#include "src/deprecated/libfred/contact_verification/contact_verification_state.hh"
#include "src/deprecated/libfred/object_states.hh"
#include "src/deprecated/libfred/public_request/public_request_impl.hh"
#include "src/util/cfg/config_handler.hh"
#include "src/util/cfg/config_handler_decl.hh"
#include "src/util/cfg/handle_messenger_args.hh"
#include "src/util/cfg/handle_registry_args.hh"
#include "src/util/types/birthdate.hh"

#include "libfred/mailer.hh"
#include "libfred/public_request/public_request_on_status_action.hh"

#include "libhermes/libhermes.hh"

#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

namespace Corba {
namespace Admin {

namespace {

class MojeIdPublicRequestBase : public LibFred::PublicRequest::PublicRequestAuthImpl
{
public:
    void sendPasswords() override
    {
        throw std::runtime_error(std::string("method ") + __PRETTY_FUNCTION__ + " should never be called");
    }

    std::string generatePasswords() override
    {
        throw std::runtime_error(std::string("method ") + __PRETTY_FUNCTION__ + " should never be called");
    }
};

class MojeIdContactValidation : public LibFred::PublicRequest::PublicRequestImpl
{
public:
    bool check() const override
    {
        return true;
    }

    void invalidateAction() override
    {
        FREDLOG_DEBUG(boost::format("invalidation request id=%1%") % this->getId());
    }

    void processAction(bool _check [[gnu::unused]]) override
    {
        FREDLOG_DEBUG(boost::format("processing validation request id=%1%") % this->getId());

        Database::Connection conn = Database::Manager::acquire();
        Database::Transaction tx(conn);

        const unsigned long long oid = this->getObject(0).id;

        const LibFred::Contact::Verification::State contact_state =
                LibFred::Contact::Verification::get_contact_verification_state(oid);
        if (contact_state.has_all(LibFred::Contact::Verification::State::ciVm) ||
                !contact_state.has_all(LibFred::Contact::Verification::State::CivM))
        {
            throw LibFred::PublicRequest::NotApplicable("pre_insert_checks: failed!");
        }

        /* set new state */
        LibFred::PublicRequest::insertNewStateRequest(this->getId(), oid, "validatedContact");
        LibFred::update_object_states(oid);

        tx.commit();
    }

    void fillTemplateParams(LibFred::Mailer::Parameters&) const override
    {
    }

    std::string getTemplateName() const override
    {
        return std::string{};
    }

    void save() override
    {
        FREDLOG_DEBUG("save");
        if (this->getId() == 0)
        {
            throw std::runtime_error("insert new request disabled");
        }
        PublicRequestImpl::save();
        Database::Connection conn = Database::Manager::acquire();
        conn.exec_params(
            "UPDATE public_request SET on_status_action = $1::enum_on_status_action_type"
            " WHERE id = $2::bigint",
            Database::query_param_list
                (Conversion::Enums::to_db_handle(LibFred::PublicRequest::OnStatusAction::scheduled))
                (this->getId()));
    }
};

} // namespace Corba::Admin::{anonymous}

} // namespace Corba::Admin
} // namespace Corba

using namespace Corba::Admin;

LibFred::PublicRequest::Factory&
Corba::Admin::add_producers(LibFred::PublicRequest::Factory& factory)
{
    return factory
            .add_producer({PRT_MOJEID_CONTACT_CONDITIONAL_IDENTIFICATION,
                           LibFred::PublicRequest::make_public_request_producer<MojeIdPublicRequestBase>()})
            .add_producer({PRT_MOJEID_CONTACT_IDENTIFICATION,
                           LibFred::PublicRequest::make_public_request_producer<MojeIdPublicRequestBase>()})
            .add_producer({PRT_MOJEID_CONTACT_VALIDATION,
                           LibFred::PublicRequest::make_public_request_producer<MojeIdContactValidation>()})
            .add_producer({PRT_MOJEID_CONDITIONALLY_IDENTIFIED_CONTACT_TRANSFER,
                           LibFred::PublicRequest::make_public_request_producer<MojeIdPublicRequestBase>()})
            .add_producer({PRT_MOJEID_IDENTIFIED_CONTACT_TRANSFER,
                           LibFred::PublicRequest::make_public_request_producer<MojeIdPublicRequestBase>()})
            .add_producer({PRT_MOJEID_CONTACT_REIDENTIFICATION,
                           LibFred::PublicRequest::make_public_request_producer<MojeIdPublicRequestBase>()})
            .add_producer({PRT_MOJEID_CONTACT_PREVALIDATED_UNIDENTIFIED_TRANSFER,
                           LibFred::PublicRequest::make_public_request_producer<MojeIdPublicRequestBase>()})
            .add_producer({PRT_MOJEID_CONTACT_PREVALIDATED_TRANSFER,
                           LibFred::PublicRequest::make_public_request_producer<MojeIdPublicRequestBase>()});
}
