/*
 * Copyright (C) 2008-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "corba/Admin.hh"

#include "src/backend/admin/registrar/create_registrar.hh"
#include "src/backend/admin/registrar/update_epp_auth.hh"
#include "src/backend/admin/registrar/update_registrar.hh"
#include "src/backend/admin/registrar/update_zone_access.hh"
#include "src/bin/corba/admin/common.hh"
#include "src/bin/corba/admin/public_request_mojeid.hh"
#include "src/bin/corba/admin/session_impl.hh"
#include "src/bin/corba/admin/usertype_conv.hh"
#include "src/bin/corba/connection_releaser.hh"
#include "src/bin/corba/util/corba_conversions_string.hh"
#include "src/deprecated/util/dbsql.hh"
#include "src/deprecated/libfred/registry.hh"
#include "src/deprecated/libfred/registrar.hh"
#include "src/bin/corba/admin/usertype_conv.hh"
#include "src/bin/corba/admin/common.hh"
#include "src/deprecated/libfred/public_request/public_request_authinfo_impl.hh"
#include "src/deprecated/libfred/public_request/public_request_block_impl.hh"
#include "src/deprecated/libfred/public_request/public_request_personal_info_impl.hh"
#include "util/log/log.hh"
#include "util/util.hh"

#include <boost/optional.hpp>
#include <boost/optional/optional_io.hpp>

#include <math.h>
#include <memory>
#include <iomanip>
#include <algorithm>
#include <utility>
#include <ostream>
#include <set>

ccReg_Session_i::ccReg_Session_i(const std::string& _session_id,
                                 const std::string& database [[gnu::unused]],
                                 NameService *ns,

                                 bool restricted_handles,
                                 unsigned adifd_session_timeout,

                                 ccReg::BankingInvoicing_ptr _banking,
                                 ccReg_User_i* _user)
                               : session_id_(_session_id),
                                 restricted_handles_(restricted_handles),
                                 adifd_session_timeout_(adifd_session_timeout),
                                 m_ns (ns),
                                 m_banking_invoicing(_banking),
                                 m_user(_user),
                                 m_fm_client(ns),
                                 m_last_activity(second_clock::local_time()),
                                 db_disconnect_guard_ ()
{
    Database::Connection conn = Database::Manager::acquire();
    db_disconnect_guard_.reset(new DB(conn));

  FREDLOG_SET_CONTEXT(LogContext, ctx, session_id_);

  m_registry_manager.reset(LibFred::Manager::create(db_disconnect_guard_,
                                                     restricted_handles_));

  m_registry_manager->dbManagerInit();
  m_registry_manager->initStates();

  m_publicrequest_manager.reset(LibFred::PublicRequest::Manager::create(m_registry_manager->getDomainManager(),
                                                                         m_registry_manager->getContactManager(),
                                                                         m_registry_manager->getNssetManager(),
                                                                         m_registry_manager->getKeysetManager()));
  m_invoicing_manager.reset(LibFred::Invoicing::Manager::create());

  m_banking_manager.reset(LibFred::Banking::Manager::create());

  m_domains = new ccReg_Domains_i(m_registry_manager->getDomainManager()->createList(), &settings_);
  m_contacts = new ccReg_Contacts_i(m_registry_manager->getContactManager()->createList(), &settings_);
  m_nssets = new ccReg_NSSets_i(m_registry_manager->getNssetManager()->createList(), &settings_);
  m_keysets = new ccReg_KeySets_i(m_registry_manager->getKeysetManager()->createList(), &settings_);
  m_registrars = new ccReg_Registrars_i(m_registry_manager->getRegistrarManager()->createList()
          ,m_registry_manager->getZoneManager()->createList());
  m_invoices = new ccReg_Invoices_i(m_invoicing_manager->createList());
  m_filters = new ccReg_Filters_i(m_registry_manager->getFilterManager()->getList());
  m_publicrequests = new ccReg_PublicRequests_i(m_publicrequest_manager->createList());
  m_zones = new ccReg_Zones_i(m_registry_manager->getZoneManager()->createList());

  m_registrars->setDB();
  m_contacts->setDB();
  m_domains->setDB();
  m_nssets->setDB();
  m_keysets->setDB();
  m_publicrequests->setDB();
  m_invoices->setDB();
  m_zones->setDB();

  settings_.set("filter.history", "off");

  updateActivity();
}

ccReg_Session_i::~ccReg_Session_i()
{
  FREDLOG_SET_CONTEXT(LogContext, ctx, session_id_);
  FREDLOG_TRACE("[CALL] ccReg_Session_i::~ccReg_Session_i()");

  delete m_registrars;
  delete m_domains;
  delete m_contacts;
  delete m_nssets;
  delete m_keysets;
  delete m_publicrequests;
  delete m_invoices;
  delete m_filters;
  delete m_user;
  delete m_zones;
}

Registry::User_ptr ccReg_Session_i::getUser()
{
  FREDLOG_SET_CONTEXT(LogContext, ctx, base_context_);
  ConnectionReleaser releaser;

  return m_user->_this();
}

Registry::PageTable_ptr ccReg_Session_i::getPageTable(ccReg::FilterType _type)
{
  FREDLOG_SET_CONTEXT(LogContext, ctx, base_context_);
  ConnectionReleaser releaser;

  FREDLOG_TRACE(boost::format("[CALL] ccReg_Session_i::getPageTable(%1%)") % _type);
  switch (_type)
  {
    case ccReg::FT_FILTER:
      return m_filters->_this();
    case ccReg::FT_REGISTRAR:
      return m_registrars->_this();
    case ccReg::FT_OBJ:
      return Registry::PageTable::_nil();
    case ccReg::FT_CONTACT:
      return m_contacts->_this();
    case ccReg::FT_NSSET:
      return m_nssets->_this();
    case ccReg::FT_KEYSET:
      return m_keysets->_this();
    case ccReg::FT_DOMAIN:
      return m_domains->_this();
    case ccReg::FT_INVOICE:
      return m_invoices->_this();
    case ccReg::FT_PUBLICREQUEST:
      return m_publicrequests->_this();
    case ccReg::FT_MAIL:
      throw ccReg::Admin::ServiceUnavailable();
    case ccReg::FT_FILE:
      throw ccReg::Admin::ServiceUnavailable();
    case ccReg::FT_LOGGER:
      throw ccReg::Admin::ServiceUnavailable();
    case ccReg::FT_ZONE:
      return m_zones->_this();
    case ccReg::FT_MESSAGE:
      throw ccReg::Admin::ServiceUnavailable();
    default:
      break;
  }
  FREDLOG_DEBUG(boost::format("[ERROR] ccReg_Session_i::getPageTable(%1%): unknown type specified") % _type);
  return Registry::PageTable::_nil();
}

CORBA::Any* ccReg_Session_i::getDetail(ccReg::FilterType _type, ccReg::TID _id)
{
  try
  {
    FREDLOG_SET_CONTEXT(LogContext, ctx, base_context_);
    ConnectionReleaser releaser;

    FREDLOG_TRACE(boost::format("[CALL] ccReg_Session_i::getDetail(%1%, %2%)") % _type % _id);
    CORBA::Any* const result = new CORBA::Any;

    switch (_type)
    {
    case ccReg::FT_CONTACT:
      *result <<= getContactDetail(_id);
      break;

    case ccReg::FT_NSSET:
      *result <<= getNSSetDetail(_id);
      break;

    case ccReg::FT_KEYSET:
      *result <<= getKeySetDetail(_id);
      break;

    case ccReg::FT_DOMAIN:
      *result <<= getDomainDetail(_id);
      break;

    case ccReg::FT_REGISTRAR:
      *result <<= getRegistrarDetail(_id);
      break;

    case ccReg::FT_PUBLICREQUEST:
      *result <<= getPublicRequestDetail(_id);
      break;

    case ccReg::FT_INVOICE:
      *result <<= getInvoiceDetail(_id);
      break;

    case ccReg::FT_MAIL:
      FREDLOG_ERROR("Unimplemented filter type used in getDetail(): FT_MAIL");
      break;

    case ccReg::FT_LOGGER:
      FREDLOG_ERROR("Unimplemented filter type used in getDetail(): FT_LOGGER");
      break;

    case ccReg::FT_MESSAGE:
      FREDLOG_ERROR("Unimplemented filter type used in getDetail(): FT_MESSAGE");
      break;

    case ccReg::FT_SESSION:
      FREDLOG_ERROR("Unimplemented filter type used in getDetail(): FT_SESSION");
      break;
    case ccReg::FT_ZONE:
      FREDLOG_ERROR("Unimplemented filter type used in getDetail(): FT_ZONE");
      break;
    case ccReg::FT_FILE:
      FREDLOG_ERROR("Unimplemented filter type used in getDetail(): FT_FILE");
      break;
    case ccReg::FT_FILTER:
      FREDLOG_ERROR("Unimplemented filter type used in getDetail(): FT_FILTER");
      break;
    case ccReg::FT_OBJ:
      FREDLOG_ERROR("Unimplemented filter type used in getDetail(): FT_OBJ");
      break;
    default:
      FREDLOG_ERROR("Invalid filter type used in getDetail()");
      break;
    }

    return result;
  }
  catch (const Registry::SqlQueryTimeout&)
  {
    FREDLOG_ERROR("ccReg_Session_i::getDetail ex: Registry::SqlQueryTimeout");
    throw;
  }
  catch (const ccReg::Admin::ServiceUnavailable&)
  {
    FREDLOG_ERROR("ccReg_Session_i::getDetail ex: ccReg::Admin::ServiceUnavailable");
    throw;
  }
  catch (const ccReg::Admin::ObjectNotFound&)
  {
    FREDLOG_WARNING("ccReg_Session_i::getDetail ex: ccReg::Admin::ObjectNotFound");
    throw;
  }
  catch(std::exception& ex)
  {
    FREDLOG_ERROR(boost::format("ccReg_Session_i::getDetail ex: %1%") % ex.what());
    throw ccReg::Admin::ServiceUnavailable();
  }
  catch (...)
  {
    FREDLOG_ERROR("ccReg_Session_i::getDetail unknown exception");
    throw ccReg::Admin::ServiceUnavailable();
  }
}

const std::string& ccReg_Session_i::getId() const
{
  return session_id_;
}

ccReg::BankingInvoicing_ptr ccReg_Session_i::getBankingInvoicing()
{
    return ccReg::BankingInvoicing::_duplicate(m_banking_invoicing);
}

void ccReg_Session_i::updateActivity()
{
  ptime tmp = m_last_activity;
  m_last_activity = second_clock::local_time();
  FREDLOG_DEBUG(boost::format("session activity update: %1% (was %2%)")
      % m_last_activity % tmp);
}

bool ccReg_Session_i::isTimeouted() const
{
  ptime threshold = second_clock::local_time() - seconds(adifd_session_timeout_);
  bool timeout = m_last_activity < threshold;
  FREDLOG_DEBUG(boost::format("session `%1%' will timeout in %2% -- session %3%")
                                      % session_id_
                                      % to_simple_string(m_last_activity - threshold)
                                      % (timeout ? "timeout" : "alive"));
  return timeout;
}

const ptime& ccReg_Session_i::getLastActivity() const
{
  return m_last_activity;
}

Registry::Domain::Detail* ccReg_Session_i::getDomainDetail(ccReg::TID _id)
{
    FREDLOG_DEBUG(boost::format("constructing domain filter for object id=%1%' detail") % _id);
    std::unique_ptr<LibFred::Domain::List>
        tmp_domain_list(m_registry_manager->getDomainManager()->createList());

    Database::Filters::Union uf(&settings_);
    Database::Filters::Domain *filter = new Database::Filters::DomainHistoryImpl();
    filter->addId().setValue(Database::ID(_id));
    uf.addFilter(filter);

    tmp_domain_list->reload(uf);
    unsigned filter_count = tmp_domain_list->getCount();
    if (filter_count > 0)
    {
      return createHistoryDomainDetail(tmp_domain_list.get());
    }
    throw ccReg::Admin::ObjectNotFound();
}

Registry::Contact::Detail* ccReg_Session_i::getContactDetail(ccReg::TID _id)
{
    FREDLOG_DEBUG(boost::format("constructing contact filter for object id=%1%' detail") % _id);
    std::unique_ptr<LibFred::Contact::List>
        tmp_contact_list(m_registry_manager->getContactManager()->createList());

    Database::Filters::Union uf(&settings_);
    Database::Filters::Contact *filter = new Database::Filters::ContactHistoryImpl();
    filter->addId().setValue(Database::ID(_id));
    uf.addFilter(filter);

    tmp_contact_list->reload(uf);

    unsigned filter_count = tmp_contact_list->getCount();
    if (filter_count > 0)
    {
      return createHistoryContactDetail(tmp_contact_list.get());
    }
    throw ccReg::Admin::ObjectNotFound();
}

Registry::NSSet::Detail* ccReg_Session_i::getNSSetDetail(ccReg::TID _id)
{
    FREDLOG_DEBUG(boost::format("constructing nsset filter for object id=%1%' detail") % _id);
    std::unique_ptr<LibFred::Nsset::List>
        tmp_nsset_list(m_registry_manager->getNssetManager()->createList());

    Database::Filters::Union uf(&settings_);
    Database::Filters::NSSet *filter = new Database::Filters::NSSetHistoryImpl();
    filter->addId().setValue(Database::ID(_id));
    uf.addFilter(filter);

    tmp_nsset_list->reload(uf);

    unsigned filter_count = tmp_nsset_list->getCount();
    if (filter_count > 0)
    {
      return createHistoryNSSetDetail(tmp_nsset_list.get());
    }
    throw ccReg::Admin::ObjectNotFound();
}

Registry::KeySet::Detail* ccReg_Session_i::getKeySetDetail(ccReg::TID _id)
{
        FREDLOG_DEBUG(boost::format("constructing keyset filter for object id=%1%' detail") % _id);

        std::unique_ptr <LibFred::Keyset::List>
            tmp_keyset_list(m_registry_manager->getKeysetManager()->createList());

        Database::Filters::Union uf(&settings_);
        Database::Filters::KeySet *filter = new Database::Filters::KeySetHistoryImpl();
        filter->addId().setValue(Database::ID(_id));
        uf.addFilter(filter);

        tmp_keyset_list->reload(uf);

        unsigned filter_count = tmp_keyset_list->getCount();
        if (filter_count > 0)
        {
          return createHistoryKeySetDetail(tmp_keyset_list.get());
        }
        throw ccReg::Admin::ObjectNotFound();
}

Registry::Registrar::Detail* ccReg_Session_i::getRegistrarDetail(ccReg::TID _id)
{
    FREDLOG_DEBUG(boost::format("constructing registrar filter for object id=%1%' detail") % _id);
    LibFred::Registrar::RegistrarList::AutoPtr tmp_registrar_list =
        m_registry_manager->getRegistrarManager()->createList();

    Database::Filters::Union uf;
    Database::Filters::Registrar *filter = new Database::Filters::RegistrarImpl();
    filter->addId().setValue(Database::ID(_id));
    uf.addFilter(filter);

    tmp_registrar_list->reload(uf);

    if (tmp_registrar_list->size() != 1)
    {
      throw ccReg::Admin::ObjectNotFound();
    }
    return createRegistrarDetail(tmp_registrar_list->get(0));
}

Registry::PublicRequest::Detail* ccReg_Session_i::getPublicRequestDetail(ccReg::TID _id)
{
    FREDLOG_DEBUG(boost::format("constructing public request filter for object id=%1%' detail") % _id);
    std::unique_ptr<LibFred::PublicRequest::List> tmp_request_list(m_publicrequest_manager->createList());

    Database::Filters::Union union_filter;
    Database::Filters::PublicRequest *filter = new Database::Filters::PublicRequestImpl();
    filter->addId().setValue(Database::ID(_id));
    union_filter.addFilter(filter);

    tmp_request_list->reload(union_filter);

    if (tmp_request_list->size() != 1)
    {
      throw ccReg::Admin::ObjectNotFound();
    }
    return createPublicRequestDetail(tmp_request_list->get(0));
}

Registry::Invoicing::Detail* ccReg_Session_i::getInvoiceDetail(ccReg::TID _id)
{
    FREDLOG_DEBUG(boost::format("constructing invoice filter for object id=%1%' detail") % _id);
    std::unique_ptr<LibFred::Invoicing::List> tmp_invoice_list(m_invoicing_manager->createList());

    Database::Filters::Union union_filter;
    Database::Filters::Invoice *filter = new Database::Filters::InvoiceImpl();
    filter->addId().setValue(Database::ID(_id));
    union_filter.addFilter(filter);

    tmp_invoice_list->reload(union_filter);

    if (tmp_invoice_list->getSize() != 1)
    {
      throw ccReg::Admin::ObjectNotFound();
    }
    return createInvoiceDetail(tmp_invoice_list->get(0));
}

Registry::Zone::Detail* ccReg_Session_i::getZoneDetail(ccReg::TID _id)
{
    FREDLOG_DEBUG(boost::format("constructing zone filter for object id=%1%' detail") % _id);
    LibFred::Zone::Manager::ZoneListPtr tmp_zone_list =
        m_registry_manager->getZoneManager()->createList();

    Database::Filters::Union uf;
    Database::Filters::Zone *filter = new Database::Filters::ZoneImpl();
    filter->addId().setValue(Database::ID(_id));
    uf.addFilter(filter);

    tmp_zone_list->reload(uf);

    if (tmp_zone_list->size() != 1)
    {
      throw ccReg::Admin::ObjectNotFound();
    }
    return createZoneDetail(dynamic_cast<LibFred::Zone::Zone*>(tmp_zone_list->get(0)));
}

Registry::Domain::Detail* ccReg_Session_i::createHistoryDomainDetail(LibFred::Domain::List* _list)
{
  FREDLOG_TRACE("[CALL] ccReg_Session_i::createHistoryDomainDetail()");
  Registry::Domain::Detail *detail = new Registry::Domain::Detail();

  /* array for object external states already assigned */
  std::vector<LibFred::TID> ext_states_ids;

  /* we going backwards because at the end there are latest data */
  for (int n = _list->size() - 1; n >= 0; --n)
  {
    LibFred::Domain::Domain *act  = _list->getDomain(n);
    LibFred::Domain::Domain *prev = ((unsigned)n == _list->size() - 1 ? act : _list->getDomain(n + 1));

    /* just copy static data */
    if (act == prev)
    {
      detail->id = act->getId();
      detail->handle = DUPSTRFUN(act->getHandle);
      detail->roid = DUPSTRFUN(act->getROID);
      detail->transferDate = DUPSTRDATE(act->getTransferDate);
      detail->updateDate = DUPSTRDATE(act->getUpdateDate);
      detail->createDate = DUPSTRDATE(act->getCreateDate);
      /**
       * we want to display date of past deletion or future "cancel" date value
       * it is what getCancelDate() method do
       */
      detail->deleteDate  = DUPSTRDATE(act->getCancelDate);
      detail->outZoneDate = DUPSTRDATE(act->getOutZoneDate);

      detail->createRegistrar.id     = act->getCreateRegistrarId();
      detail->createRegistrar.handle = DUPSTRFUN(act->getCreateRegistrarHandle);
      detail->createRegistrar.type   = ccReg::FT_REGISTRAR;

      detail->updateRegistrar.id     = act->getUpdateRegistrarId();
      detail->updateRegistrar.handle = DUPSTRFUN(act->getUpdateRegistrarHandle);
      detail->updateRegistrar.type   = ccReg::FT_REGISTRAR;
    }

    /* macros are defined in common.h */

    MAP_HISTORY_OID(registrar, getRegistrarId, getRegistrarHandle, ccReg::FT_REGISTRAR);
    MAP_HISTORY_STRING(authInfo, getAuthPw);

    /* object status */
    for (unsigned s = 0; s < act->getStatusCount(); ++s)
    {
      const LibFred::Status *tmp = act->getStatusByIdx(s);

      FREDLOG_DEBUG(boost::format("history detail -- (id=%1%) checking state %2%") % tmp->getId() % tmp->getStatusId());
      std::vector<LibFred::TID>::const_iterator it = find(ext_states_ids.begin(), ext_states_ids.end(), tmp->getId());
      if (it == ext_states_ids.end())
      {
        ext_states_ids.push_back(tmp->getId());
        FREDLOG_DEBUG(boost::format("history detail -- state %1% is added for output") % tmp->getStatusId());

        unsigned sl = detail->states.length();
        detail->states.length(sl + 1);
        detail->states[sl].id    = tmp->getStatusId();
        detail->states[sl].from  = makeCorbaTime(tmp->getFrom(), true);
        detail->states[sl].to    = makeCorbaTime(tmp->getTo(), true);
      }
    }

    /* domain specific follows */
    MAP_HISTORY_OID(registrant, getRegistrantId, getRegistrantHandle, ccReg::FT_CONTACT);
    MAP_HISTORY_DATE(expirationDate, getExpirationDate);
    MAP_HISTORY_DATE(valExDate, getValExDate);
    MAP_HISTORY_BOOL(publish, getPublish);
    MAP_HISTORY_OID(nsset, getNssetId, getNssetHandle, ccReg::FT_NSSET);
    MAP_HISTORY_OID(keyset, getKeysetId, getKeysetHandle, ccReg::FT_KEYSET);

    /* admin list */
    try
    {
      bool alist_changed = (act->getAdminCount(1) != prev->getAdminCount(1)) || (act == prev);
      for (unsigned n = 0; alist_changed != true && n < act->getAdminCount(1); ++n)
      {
        if (act->getAdminIdByIdx(n, 1) != prev->getAdminIdByIdx(n, 1))
        {
          alist_changed = true;
          break;
        }
      }
      if (alist_changed)
      {
        Registry::OIDSeq oid_seq;
        oid_seq.length(act->getAdminCount(1));
        for (unsigned n = 0; n < act->getAdminCount(1); ++n)
        {
          oid_seq[n].id     = act->getAdminIdByIdx(n, 1);
          oid_seq[n].handle = DUPSTRC(act->getAdminHandleByIdx(n, 1));
          oid_seq[n].type   = ccReg::FT_CONTACT;
        }
        ADD_NEW_HISTORY_RECORD(admins, oid_seq);
      }
      else
      {
        MODIFY_LAST_HISTORY_RECORD(admins);
      }
    }
    catch (const LibFred::NOT_FOUND&)
    {
      FREDLOG_ERROR(boost::format("domain id=%1% detail lib -> CORBA: request for admin contact out of range 0..%2%")
                                           % act->getId() % act->getAdminCount(1));
    }

    /* temp list */
    try
    {
      bool tlist_changed = (act->getAdminCount(2) != prev->getAdminCount(2)) || (act == prev);
      for (unsigned n = 0; tlist_changed != true && n < act->getAdminCount(2); ++n)
      {
        if (act->getAdminIdByIdx(n, 2) != prev->getAdminIdByIdx(n, 2))
        {
          tlist_changed = true;
          break;
        }
      }
      if (tlist_changed)
      {
        Registry::OIDSeq oid_seq;
        oid_seq.length(act->getAdminCount(2));
        for (unsigned n = 0; n < act->getAdminCount(2); ++n)
        {
          oid_seq[n].id     = act->getAdminIdByIdx(n, 2);
          oid_seq[n].handle = DUPSTRC(act->getAdminHandleByIdx(n, 2));
          oid_seq[n].type   = ccReg::FT_CONTACT;
        }
        ADD_NEW_HISTORY_RECORD(temps, oid_seq);
      }
      else
      {
        MODIFY_LAST_HISTORY_RECORD(temps);
      }
    }
    catch (const LibFred::NOT_FOUND&)
    {
      FREDLOG_ERROR(boost::format("domain id=%1% detail lib -> CORBA: request for temp contact out of range 0..%2%")
                                           % act->getId() % act->getAdminCount(2));
    }
  }
  return detail;
}

Registry::Contact::Detail* ccReg_Session_i::createHistoryContactDetail(LibFred::Contact::List* _list)
{
  FREDLOG_TRACE("[CALL] ccReg_Session_i::createHistoryContactDetail()");
  Registry::Contact::Detail *detail = new Registry::Contact::Detail();

  /* array for object external states already assigned */
  std::vector<LibFred::TID> ext_states_ids;

  /* we going backwards because at the end there are latest data */
  for (int n = _list->size() - 1; n >= 0; --n)
  {
    LibFred::Contact::Contact *act  = _list->getContact(n);
    LibFred::Contact::Contact *prev = ((unsigned)n == _list->size() - 1 ? act : _list->getContact(n + 1));

    FREDLOG_DEBUG(boost::format("history detail -- iteration left %1%, history id is %2%") % n % act->getHistoryId());

    /* just copy static data */
    if (act == prev)
    {
      detail->id = act->getId();
      detail->handle = DUPSTRFUN(act->getHandle);
      detail->roid = DUPSTRFUN(act->getROID);
      detail->transferDate = DUPSTRDATE(act->getTransferDate);
      detail->updateDate = DUPSTRDATE(act->getUpdateDate);
      detail->createDate = DUPSTRDATE(act->getCreateDate);
      detail->deleteDate = DUPSTRDATE(act->getDeleteDate);

      detail->createRegistrar.id     = act->getCreateRegistrarId();
      detail->createRegistrar.handle = DUPSTRFUN(act->getCreateRegistrarHandle);
      detail->createRegistrar.type   = ccReg::FT_REGISTRAR;

      detail->updateRegistrar.id     = act->getUpdateRegistrarId();
      detail->updateRegistrar.handle = DUPSTRFUN(act->getUpdateRegistrarHandle);
      detail->updateRegistrar.type   = ccReg::FT_REGISTRAR;
    }

    /* macros are defined in common.h */

    MAP_HISTORY_OID(registrar, getRegistrarId, getRegistrarHandle, ccReg::FT_REGISTRAR);
    MAP_HISTORY_STRING(authInfo, getAuthPw);

    /* object status */
    for (unsigned s = 0; s < act->getStatusCount(); ++s)
    {
      const LibFred::Status *tmp = act->getStatusByIdx(s);
      FREDLOG_DEBUG(boost::format("history detail -- (id=%1%) checking state %2%") % tmp->getId() % tmp->getStatusId());
      std::vector<LibFred::TID>::const_iterator it = find(ext_states_ids.begin(), ext_states_ids.end(), tmp->getId());
      if (it == ext_states_ids.end())
      {
        ext_states_ids.push_back(tmp->getId());
        FREDLOG_DEBUG(boost::format("history detail -- state %1% is added for output") % tmp->getStatusId());

        unsigned sl = detail->states.length();
        detail->states.length(sl + 1);
        detail->states[sl].id    = tmp->getStatusId();
        detail->states[sl].from  = makeCorbaTime(tmp->getFrom(), true);
        detail->states[sl].to    = makeCorbaTime(tmp->getTo(), true);
      }
    }

    /* contact specific data follows */
    MAP_HISTORY_STRING(name, getName);
    MAP_HISTORY_STRING(organization, getOrganization);
    MAP_HISTORY_STRING(street1, getStreet1);
    MAP_HISTORY_STRING(street2, getStreet2);
    MAP_HISTORY_STRING(street3, getStreet3);
    MAP_HISTORY_STRING(province, getProvince);
    MAP_HISTORY_STRING(postalcode, getPostalCode);
    MAP_HISTORY_STRING(city, getCity);
    MAP_HISTORY_STRING(country, getCountry);
    MAP_HISTORY_STRING(telephone, getTelephone);
    MAP_HISTORY_STRING(fax, getFax);
    MAP_HISTORY_STRING(email, getEmail);
    MAP_HISTORY_STRING(notifyEmail, getNotifyEmail);

    /* TODO: rename `SSN' methods in library to `ident' */
    MAP_HISTORY_STRING(ident, getSSN);
    MAP_HISTORY_STRING(identType, getSSNType);

    MAP_HISTORY_STRING(vat, getVAT);
    MAP_HISTORY_BOOL(discloseName, getDiscloseName);
    MAP_HISTORY_BOOL(discloseOrganization, getDiscloseOrganization);
    MAP_HISTORY_BOOL(discloseEmail, getDiscloseEmail);
    MAP_HISTORY_BOOL(discloseAddress, getDiscloseAddr);
    MAP_HISTORY_BOOL(discloseTelephone, getDiscloseTelephone);
    MAP_HISTORY_BOOL(discloseFax, getDiscloseFax);
    MAP_HISTORY_BOOL(discloseIdent, getDiscloseIdent);
    MAP_HISTORY_BOOL(discloseVat, getDiscloseVat);
    MAP_HISTORY_BOOL(discloseNotifyEmail, getDiscloseNotifyEmail);

    /* address list */
    try
    {
      bool addr_list_changed = (act->getAddressCount() != prev->getAddressCount()) || (act == prev);
      if (!addr_list_changed)
      {
        for (unsigned i = 0; i < act->getAddressCount(); ++i)
        {
          if (*(act->getAddressByIdx(i)) != *(prev->getAddressByIdx(i)))
          {
            addr_list_changed = true;
            break;
          }
        }
      }
      if (addr_list_changed)
      {
        Registry::Contact::AddressSeq addresses;
        addresses.length(act->getAddressCount());
        for (unsigned k = 0; k < act->getAddressCount(); ++k)
        {
          addresses[k].type        = DUPSTRFUN(act->getAddressByIdx(k)->getType);
          addresses[k].companyName = DUPSTRFUN(act->getAddressByIdx(k)->getCompanyName);
          addresses[k].street1     = DUPSTRFUN(act->getAddressByIdx(k)->getStreet1);
          addresses[k].street2     = DUPSTRFUN(act->getAddressByIdx(k)->getStreet2);
          addresses[k].street3     = DUPSTRFUN(act->getAddressByIdx(k)->getStreet3);
          addresses[k].province    = DUPSTRFUN(act->getAddressByIdx(k)->getProvince);
          addresses[k].postalcode  = DUPSTRFUN(act->getAddressByIdx(k)->getPostalCode);
          addresses[k].city        = DUPSTRFUN(act->getAddressByIdx(k)->getCity);
          addresses[k].country     = DUPSTRFUN(act->getAddressByIdx(k)->getCountry);
          FREDLOG_DEBUG(boost::format("contact id=%1% add address to output type=%2%")
                  % act->getId() % act->getAddressByIdx(k)->getType());
        }
        ADD_NEW_HISTORY_RECORD(addresses, addresses);
      }
      else
      {
        MODIFY_LAST_HISTORY_RECORD(addresses);
      }
    }
    catch (const LibFred::NOT_FOUND&)
    {
      FREDLOG_ERROR(boost::format("contact id=%1% detail lib -> CORBA: request for address out of range 0..%2%")
                                           % act->getId() % act->getAddressCount());
    }
  }
  return detail;
}

Registry::NSSet::Detail* ccReg_Session_i::createHistoryNSSetDetail(LibFred::Nsset::List* _list)
{
  FREDLOG_TRACE("[CALL] ccReg_Session_i::createHistoryNSSetDetail()");
  Registry::NSSet::Detail *detail = new Registry::NSSet::Detail();

  /* array for object external states already assigned */
  std::vector<LibFred::TID> ext_states_ids;

  /* we going backwards because at the end there are latest data */
  for (int n = _list->size() - 1; n >= 0; --n)
  {
    LibFred::Nsset::Nsset *act  = _list->getNsset(n);
    LibFred::Nsset::Nsset *prev = ((unsigned)n == _list->size() - 1 ? act : _list->getNsset(n + 1));

    /* just copy static data */
    if (act == prev)
    {
      detail->id = act->getId();
      detail->handle = DUPSTRFUN(act->getHandle);
      detail->roid = DUPSTRFUN(act->getROID);
      detail->transferDate = DUPSTRDATE(act->getTransferDate);
      detail->updateDate = DUPSTRDATE(act->getUpdateDate);
      detail->createDate = DUPSTRDATE(act->getCreateDate);
      detail->deleteDate = DUPSTRDATE(act->getDeleteDate);

      detail->createRegistrar.id     = act->getCreateRegistrarId();
      detail->createRegistrar.handle = DUPSTRFUN(act->getCreateRegistrarHandle);
      detail->createRegistrar.type   = ccReg::FT_REGISTRAR;

      detail->updateRegistrar.id     = act->getUpdateRegistrarId();
      detail->updateRegistrar.handle = DUPSTRFUN(act->getUpdateRegistrarHandle);
      detail->updateRegistrar.type   = ccReg::FT_REGISTRAR;
    }

    /* macros are defined in common.h */

    MAP_HISTORY_OID(registrar, getRegistrarId, getRegistrarHandle, ccReg::FT_REGISTRAR);
    MAP_HISTORY_STRING(authInfo, getAuthPw);

    /* object status */
    for (unsigned s = 0; s < act->getStatusCount(); ++s)
    {
      const LibFred::Status *tmp = act->getStatusByIdx(s);

      FREDLOG_DEBUG(boost::format("history detail -- (id=%1%) checking state %2%") % tmp->getId() % tmp->getStatusId());
      std::vector<LibFred::TID>::const_iterator it = find(ext_states_ids.begin(), ext_states_ids.end(), tmp->getId());
      if (it == ext_states_ids.end())
      {
        ext_states_ids.push_back(tmp->getId());
        FREDLOG_DEBUG(boost::format("history detail -- state %1% is added for output") % tmp->getStatusId());

        unsigned sl = detail->states.length();
        detail->states.length(sl + 1);
        detail->states[sl].id    = tmp->getStatusId();
        detail->states[sl].from  = makeCorbaTime(tmp->getFrom(), true);
        detail->states[sl].to    = makeCorbaTime(tmp->getTo(), true);
      }
    }

    /* nsset specific data follows */

    /* admin list */
    try
    {
      bool alist_changed = (act->getAdminCount() != prev->getAdminCount()) || (act == prev);
      for (unsigned n = 0; alist_changed != true && n < act->getAdminCount(); ++n)
      {
        if (act->getAdminIdByIdx(n) != prev->getAdminIdByIdx(n))
        {
          alist_changed = true;
          break;
        }
      }
      if (alist_changed)
      {
        Registry::OIDSeq oid_seq;
        oid_seq.length(act->getAdminCount());
        for (unsigned n = 0; n < act->getAdminCount(); ++n)
        {
          oid_seq[n].id     = act->getAdminIdByIdx(n);
          oid_seq[n].handle = DUPSTRC(act->getAdminHandleByIdx(n));
          oid_seq[n].type   = ccReg::FT_CONTACT;
        }
        ADD_NEW_HISTORY_RECORD(admins, oid_seq);
      }
      else
      {
        MODIFY_LAST_HISTORY_RECORD(admins);
      }

      MAP_HISTORY_STRING(reportLevel, getCheckLevel);
    }
    catch (const LibFred::NOT_FOUND&)
    {
      FREDLOG_ERROR(boost::format("nsset id=%1% detail lib -> CORBA: request for admin contact out of range 0..%2%")
                                           % act->getId() % act->getAdminCount());
    }

    /* dns host list */
    try
    {
      bool hlist_changed = (act->getHostCount() != prev->getHostCount()) || (act == prev);
      for (unsigned i = 0; hlist_changed != true && i < act->getHostCount(); ++i)
      {
        if (*(act->getHostByIdx(i)) != *(prev->getHostByIdx(i)))
        {
          hlist_changed = true;
          break;
        }
      }
      if (hlist_changed)
      {
        ccReg::DNSHost dns_hosts;
        dns_hosts.length(act->getHostCount());
        for (unsigned k = 0; k < act->getHostCount(); ++k)
        {
          dns_hosts[k].fqdn = DUPSTRFUN(act->getHostByIdx(k)->getName);
          dns_hosts[k].inet.length(act->getHostByIdx(k)->getAddrCount());
          FREDLOG_DEBUG(boost::format("nsset id=%1% detail lib -> CORBA: dns host `%2%' has %3% ip addresses")
                                               % act->getId() % act->getHostByIdx(k)->getName() % act->getHostByIdx(k)->getAddrCount());
          for (unsigned j = 0; j < act->getHostByIdx(k)->getAddrCount(); ++j)
          {
            dns_hosts[k].inet[j] = DUPSTRC(act->getHostByIdx(k)->getAddrByIdx(j));
          }
        }
        ADD_NEW_HISTORY_RECORD(hosts, dns_hosts);
      }
      else
      {
        MODIFY_LAST_HISTORY_RECORD(hosts);
      }
    }
    catch (const LibFred::NOT_FOUND&)
    {
      FREDLOG_ERROR(boost::format("nsset id=%1% detail lib -> CORBA: request for host out of range 0..%2%")
                                           % act->getId() % act->getHostCount());
    }
  }
  return detail;
}

Registry::KeySet::Detail* ccReg_Session_i::createHistoryKeySetDetail(LibFred::Keyset::List* _list)
{
  FREDLOG_TRACE("[CALL] ccReg_Session_i::createHistoryKeySetDetail()");
  Registry::KeySet::Detail *detail = new Registry::KeySet::Detail();

  /* array for object external states already assigned */
  std::vector<LibFred::TID> ext_states_ids;

  /* we going backwards because at the end there are latest data */
  for (int n = _list->size() - 1; n >= 0; --n)
  {
    LibFred::Keyset::Keyset *act  = _list->getKeyset(n);
    LibFred::Keyset::Keyset *prev = ((unsigned)n == _list->size() - 1 ? act : _list->getKeyset(n + 1));

    /* just copy static data */
    if (act == prev)
    {
      detail->id = act->getId();
      detail->handle = DUPSTRFUN(act->getHandle);
      detail->roid = DUPSTRFUN(act->getROID);
      detail->transferDate = DUPSTRDATE(act->getTransferDate);
      detail->updateDate = DUPSTRDATE(act->getUpdateDate);
      detail->createDate = DUPSTRDATE(act->getCreateDate);
      detail->deleteDate = DUPSTRDATE(act->getDeleteDate);

      detail->createRegistrar.id     = act->getCreateRegistrarId();
      detail->createRegistrar.handle = DUPSTRFUN(act->getCreateRegistrarHandle);
      detail->createRegistrar.type   = ccReg::FT_REGISTRAR;

      detail->updateRegistrar.id     = act->getUpdateRegistrarId();
      detail->updateRegistrar.handle = DUPSTRFUN(act->getUpdateRegistrarHandle);
      detail->updateRegistrar.type   = ccReg::FT_REGISTRAR;
    }

    /* macros are defined in common.h */

    MAP_HISTORY_OID(registrar, getRegistrarId, getRegistrarHandle, ccReg::FT_REGISTRAR);
    MAP_HISTORY_STRING(authInfo, getAuthPw);

    /* object status */
    for (unsigned s = 0; s < act->getStatusCount(); ++s)
    {
      const LibFred::Status *tmp = act->getStatusByIdx(s);

      FREDLOG_DEBUG(boost::format("history detail -- (id=%1%) checking state %2%") % tmp->getId() % tmp->getStatusId());
      std::vector<LibFred::TID>::const_iterator it = find(ext_states_ids.begin(), ext_states_ids.end(), tmp->getId());
      if (it == ext_states_ids.end())
      {
        ext_states_ids.push_back(tmp->getId());
        FREDLOG_DEBUG(boost::format("history detail -- state %1% is added for output") % tmp->getStatusId());

        unsigned sl = detail->states.length();
        detail->states.length(sl + 1);
        detail->states[sl].id    = tmp->getStatusId();
        detail->states[sl].from  = makeCorbaTime(tmp->getFrom(), true);
        detail->states[sl].to    = makeCorbaTime(tmp->getTo(), true);
      }
    }

    /* keyset specific data follows */

    /* admin list */
    try
    {
      bool alist_changed = (act->getAdminCount() != prev->getAdminCount()) || (act == prev);
      for (unsigned n = 0; alist_changed != true && n < act->getAdminCount(); ++n)
      {
        if (act->getAdminIdByIdx(n) != prev->getAdminIdByIdx(n))
        {
          alist_changed = true;
          break;
        }
      }
      if (alist_changed)
      {
        Registry::OIDSeq oid_seq;
        oid_seq.length(act->getAdminCount());
        for (unsigned n = 0; n < act->getAdminCount(); ++n)
        {
          oid_seq[n].id     = act->getAdminIdByIdx(n);
          oid_seq[n].handle = DUPSTRC(act->getAdminHandleByIdx(n));
          oid_seq[n].type   = ccReg::FT_CONTACT;
        }
        ADD_NEW_HISTORY_RECORD(admins, oid_seq);
      }
      else
      {
        MODIFY_LAST_HISTORY_RECORD(admins);
      }
    }
    catch (const LibFred::NOT_FOUND&)
    {
      FREDLOG_ERROR(boost::format("keyset id=%1% detail lib -> CORBA: request for admin contact out of range 0..%2%")
                                           % act->getId() % act->getAdminCount());
    }

    /* dsrecord list */
    try
    {
      bool dslist_changed = (act->getDSRecordCount() != prev->getDSRecordCount()) || (act == prev);
      for (unsigned i = 0; dslist_changed != true && i < act->getDSRecordCount(); ++i)
      {
        if (*(act->getDSRecordByIdx(i)) != *(prev->getDSRecordByIdx(i)))
        {
          dslist_changed = true;
          break;
        }
      }
      if (dslist_changed)
      {
        ccReg::DSRecord dsrecords;
        dsrecords.length(act->getDSRecordCount());
        for (unsigned k = 0; k < act->getDSRecordCount(); ++k)
        {
          dsrecords[k].keyTag     = act->getDSRecordByIdx(k)->getKeyTag();
          dsrecords[k].alg        = act->getDSRecordByIdx(k)->getAlg();
          dsrecords[k].digestType = act->getDSRecordByIdx(k)->getDigestType();
          dsrecords[k].digest     = DUPSTRFUN(act->getDSRecordByIdx(k)->getDigest);
          dsrecords[k].maxSigLife = act->getDSRecordByIdx(k)->getMaxSigLife();
        }
        ADD_NEW_HISTORY_RECORD(dsrecords, dsrecords);
      }
      else
      {
        MODIFY_LAST_HISTORY_RECORD(dsrecords);
      }
    }
    catch (const LibFred::NOT_FOUND&)
    {
      FREDLOG_ERROR(boost::format("keyset id=%1% detail lib -> CORBA: request for dsrecord out of range 0..%2%")
                                           % act->getId() % act->getDSRecordCount());
    }

    /* dnskey list */
    try
    {
      bool dnslist_changed = (act->getDNSKeyCount() != prev->getDNSKeyCount()) || (act == prev);
      for (unsigned i = 0; dnslist_changed != true && i < act->getDNSKeyCount(); ++i)
      {
        if (*(act->getDNSKeyByIdx(i)) != *(prev->getDNSKeyByIdx(i)))
        {
          dnslist_changed = true;
          break;
        }
      }
      if (dnslist_changed)
      {
        ccReg::DNSKey dnskeys;
        dnskeys.length(act->getDNSKeyCount());
        for (unsigned k = 0; k < act->getDNSKeyCount(); ++k)
        {
          dnskeys[k].flags     = act->getDNSKeyByIdx(k)->getFlags();
          dnskeys[k].protocol  = act->getDNSKeyByIdx(k)->getProtocol();
          dnskeys[k].alg       = act->getDNSKeyByIdx(k)->getAlg();
          dnskeys[k].key       = DUPSTRFUN(act->getDNSKeyByIdx(k)->getKey);
          FREDLOG_DEBUG(boost::format("keyset id=%1% detail lib -> CORBA: dnskey added to output "
                                              "(flags=%2% protocol=%3% alg=%4% key=%5%)")
                                              % act->getId()
                                              % act->getDNSKeyByIdx(k)->getFlags()
                                              % act->getDNSKeyByIdx(k)->getProtocol()
                                              % act->getDNSKeyByIdx(k)->getAlg()
                                              % act->getDNSKeyByIdx(k)->getKey());
        }
        ADD_NEW_HISTORY_RECORD(dnskeys, dnskeys);
      }
      else
      {
        MODIFY_LAST_HISTORY_RECORD(dnskeys);
      }
    }
    catch (const LibFred::NOT_FOUND&)
    {
      FREDLOG_ERROR(boost::format("keyset id=%1% detail lib -> CORBA: request for dnskey out of range 0..%2%")
                                           % act->getId() % act->getDNSKeyCount());
    }
  }
  return detail;
}

Registry::Registrar::Detail* ccReg_Session_i::createRegistrarDetail(LibFred::Registrar::Registrar* _registrar)
{
  FREDLOG_TRACE("[CALL] ccReg_Session_i::createRegistrarDetail()");
  FREDLOG_DEBUG(boost::format("generating registrar detail for object id=%1%") % _registrar->getId());
  Registry::Registrar::Detail *detail = new Registry::Registrar::Detail();

  detail->id = _registrar->getId();
  detail->ico = DUPSTRFUN(_registrar->getIco);
  detail->dic = DUPSTRFUN(_registrar->getDic);
  detail->varSymb = DUPSTRFUN(_registrar->getVarSymb);
  detail->vat = _registrar->getVat();
  detail->name = DUPSTRFUN(_registrar->getName);
  detail->handle = DUPSTRFUN(_registrar->getHandle);
  detail->url = DUPSTRFUN(_registrar->getURL);
  detail->organization = DUPSTRFUN(_registrar->getOrganization);
  detail->street1 = DUPSTRFUN(_registrar->getStreet1);
  detail->street2 = DUPSTRFUN(_registrar->getStreet2);
  detail->street3 = DUPSTRFUN(_registrar->getStreet3);
  detail->city = DUPSTRFUN(_registrar->getCity);
  detail->postalcode = DUPSTRFUN(_registrar->getPostalCode);
  detail->stateorprovince = DUPSTRFUN(_registrar->getProvince);
  detail->country = DUPSTRFUN(_registrar->getCountry);
  detail->telephone = DUPSTRFUN(_registrar->getTelephone);
  detail->fax = DUPSTRFUN(_registrar->getFax);
  detail->email = DUPSTRFUN(_registrar->getEmail);
  detail->credit = DUPSTRC(formatMoney(_registrar->getCredit()));
  detail->unspec_credit = DUPSTRC(formatMoney(_registrar->getCredit(0)));

  detail->access.length(_registrar->getACLSize());
  for (unsigned i = 0; i < _registrar->getACLSize(); i++)
  {
    detail->access[i].id = _registrar->getACL(i)->getId();
    detail->access[i].md5Cert = DUPSTRFUN(_registrar->getACL(i)->getCertificateMD5);
    detail->access[i].password = "";
  }
  detail->zones.length(_registrar->getRegistrarZoneSize());
  for (unsigned i = 0; i < _registrar->getRegistrarZoneSize(); i++)
  {
    detail->zones[i].id = _registrar->getRegistrarZone(i)->id;
    detail->zones[i].name = CORBA::string_dup(_registrar->getRegistrarZone(i)->name.c_str());
    detail->zones[i].credit = CORBA::string_dup(C_STR(_registrar->getRegistrarZone(i)->credit));//_registrar->getCredit(__registrar->getAZone(i)->id);
    detail->zones[i].fromDate = makeCorbaDate(_registrar->getRegistrarZone(i)->fromdate);//CORBA::string_dup(_registrar->getRegistrarZone(i)->fromdate.to_string().c_str());
    detail->zones[i].toDate = makeCorbaDate(_registrar->getRegistrarZone(i)->todate);
  }
  detail->hidden = _registrar->getSystem();
  return detail;
}

namespace {

std::vector<std::string> make_street(std::string street1, std::string street2, std::string street3)
{
    std::vector<std::string> result;
    if (!street1.empty())
    {
        if (street2.empty())
        {
            result = { std::move(street1) };
        }
        else if (street3.empty())
        {
            result = { std::move(street1), std::move(street2) };
        }
        else
        {
            result = { std::move(street1), std::move(street2), std::move(street3) };
        }
    }
    return result;
}

}//namespace {anonymous}

ccReg::TID ccReg_Session_i::updateRegistrar(const ccReg::AdminRegistrar& _registrar)
{
    try
    {
        FREDLOG_SET_CONTEXT(LogContext, ctx, base_context_ + "/updateRegistrar");
        FREDLOG_TRACE("[CALL] ccReg_Session_i::updateRegistrar()");
        ConnectionReleaser releaser;

        unsigned long long registrar_id;
        const std::string registrar_handle = LibFred::Corba::unwrap_string_from_const_char_ptr(_registrar.handle);

        std::stringstream registrar_info;
        registrar_info << "Registrar id: " << _registrar.id << " handle: " << registrar_handle;
        registrar_info << " epp auth size: " << _registrar.access.length();
        registrar_info << " zone access size: " << _registrar.zones.length();
        FREDLOG_DEBUG(registrar_info.str());

        const bool new_registrar = (_registrar.id == 0);
        if (new_registrar)
        {
            FREDLOG_TRACE("[CALL] Admin::Registrar::CreateRegistrar()");
            registrar_id = ::Admin::Registrar::create_registrar(registrar_handle,
                    LibFred::Corba::unwrap_string(_registrar.name),
                    LibFred::Corba::unwrap_string(_registrar.organization),
                    make_street(LibFred::Corba::unwrap_string(_registrar.street1),
                                LibFred::Corba::unwrap_string(_registrar.street2),
                                LibFred::Corba::unwrap_string(_registrar.street3)),
                    LibFred::Corba::unwrap_string(_registrar.city),
                    LibFred::Corba::unwrap_string(_registrar.stateorprovince),
                    LibFred::Corba::unwrap_string(_registrar.postalcode),
                    LibFred::Corba::unwrap_string(_registrar.country),
                    LibFred::Corba::unwrap_string(_registrar.telephone),
                    LibFred::Corba::unwrap_string(_registrar.fax),
                    LibFred::Corba::unwrap_string(_registrar.email),
                    LibFred::Corba::unwrap_string(_registrar.url),
                    static_cast<bool>(_registrar.hidden),
                    LibFred::Corba::unwrap_string(_registrar.ico),
                    LibFred::Corba::unwrap_string(_registrar.dic),
                    LibFred::Corba::unwrap_string(_registrar.varSymb),
                    boost::none,
                    static_cast<bool>(_registrar.vat),
                    false);
        }
        else
        {
            FREDLOG_TRACE("[CALL] Admin::Registrar::UpdateRegistrar()");
            registrar_id = _registrar.id;
            ::Admin::Registrar::update_registrar(registrar_id,
                    LibFred::Corba::unwrap_string(_registrar.handle),
                    LibFred::Corba::unwrap_string(_registrar.name),
                    LibFred::Corba::unwrap_string(_registrar.organization),
                    LibFred::Corba::unwrap_string(_registrar.street1),
                    LibFred::Corba::unwrap_string(_registrar.street2),
                    LibFred::Corba::unwrap_string(_registrar.street3),
                    LibFred::Corba::unwrap_string(_registrar.city),
                    LibFred::Corba::unwrap_string(_registrar.stateorprovince),
                    LibFred::Corba::unwrap_string(_registrar.postalcode),
                    LibFred::Corba::unwrap_string(_registrar.country),
                    LibFred::Corba::unwrap_string(_registrar.telephone),
                    LibFred::Corba::unwrap_string(_registrar.fax),
                    LibFred::Corba::unwrap_string(_registrar.email),
                    LibFred::Corba::unwrap_string(_registrar.url),
                    static_cast<bool>(_registrar.hidden),
                    LibFred::Corba::unwrap_string(_registrar.ico),
                    LibFred::Corba::unwrap_string(_registrar.dic),
                    LibFred::Corba::unwrap_string(_registrar.varSymb),
                    boost::none,
                    static_cast<bool>(_registrar.vat));
        }

        FREDLOG_TRACE("[CALL] Admin::Registrar::UpdateEppAuth()");
        Admin::Registrar::EppAuthData epp_auth_data;
        epp_auth_data.registrar_handle = registrar_handle;
        epp_auth_data.epp_auth_records.reserve(_registrar.access.length());
        for (std::size_t i = 0; i < _registrar.access.length(); ++i)
        {
            Admin::Registrar::EppAuthRecord epp_auth_record;
            epp_auth_record.id = _registrar.access[i].id;
            epp_auth_record.plain_password = LibFred::Corba::unwrap_string(_registrar.access[i].password);
            epp_auth_record.certificate_fingerprint = LibFred::Corba::unwrap_string(_registrar.access[i].md5Cert);
            epp_auth_record.new_certificate_fingerprint =
                    LibFred::Corba::unwrap_string(_registrar.access[i].md5Cert2SamePasswd);
            epp_auth_data.epp_auth_records.push_back(epp_auth_record);
        }
        Admin::Registrar::update_epp_auth(epp_auth_data);

        FREDLOG_TRACE("[CALL] Admin::Registrar::UpdateZoneAccess()");
        LibFred::Registrar::ZoneAccess::RegistrarZoneAccesses zone_accesses;
        zone_accesses.registrar_handle = registrar_handle;
        zone_accesses.zone_accesses.reserve(_registrar.zones.length());
        for (std::size_t i = 0; i < _registrar.zones.length(); ++i)
        {
            LibFred::Registrar::ZoneAccess::ZoneAccess zone_access;
            zone_access.id = _registrar.zones[i].id;
            zone_access.zone_fqdn = LibFred::Corba::unwrap_string(_registrar.zones[i].name);
            zone_access.from_date = makeBoostDate( _registrar.zones[i].fromDate);
            zone_access.to_date = makeBoostDate(_registrar.zones[i].toDate);
            zone_accesses.zone_accesses.push_back(zone_access);
        }
        Admin::Registrar::update_zone_access(zone_accesses);

        FREDLOG_DEBUG("Registrar " + registrar_handle + " was saved.");
        return registrar_id;
    }
    catch (const std::exception& e)
    {
        FREDLOG_WARNING(e.what());
        throw ccReg::Admin::UpdateFailed();
    }
}

Registry::PublicRequest::Detail* ccReg_Session_i::createPublicRequestDetail(LibFred::PublicRequest::PublicRequest* _request) {
  Registry::PublicRequest::Detail *detail = new Registry::PublicRequest::Detail();

  detail->id = _request->getId();

  switch (_request->getStatus()) {
    case LibFred::PublicRequest::PRS_OPENED:
      detail->status = Registry::PublicRequest::PRS_OPENED;
      break;
    case LibFred::PublicRequest::PRS_RESOLVED:
      detail->status = Registry::PublicRequest::PRS_RESOLVED;
      break;
    case LibFred::PublicRequest::PRS_INVALIDATED:
      detail->status = Registry::PublicRequest::PRS_INVALIDATED;
      break;
  }

  if (_request->getType() == LibFred::PublicRequest::PRT_AUTHINFO_AUTO_RIF) {
      detail->type = Registry::PublicRequest::PRT_AUTHINFO_AUTO_RIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_AUTHINFO_AUTO_PIF) {
      detail->type = Registry::PublicRequest::PRT_AUTHINFO_AUTO_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_AUTHINFO_EMAIL_PIF) {
      detail->type = Registry::PublicRequest::PRT_AUTHINFO_EMAIL_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_AUTHINFO_POST_PIF) {
      detail->type = Registry::PublicRequest::PRT_AUTHINFO_POST_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_AUTHINFO_GOVERNMENT_PIF) {
      detail->type = Registry::PublicRequest::PRT_AUTHINFO_GOVERNMENT_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_BLOCK_CHANGES_EMAIL_PIF) {
      detail->type = Registry::PublicRequest::PRT_BLOCK_CHANGES_EMAIL_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_BLOCK_CHANGES_POST_PIF) {
      detail->type = Registry::PublicRequest::PRT_BLOCK_CHANGES_POST_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_BLOCK_CHANGES_GOVERNMENT_PIF) {
      detail->type = Registry::PublicRequest::PRT_BLOCK_CHANGES_GOVERNMENT_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_BLOCK_TRANSFER_EMAIL_PIF) {
      detail->type = Registry::PublicRequest::PRT_BLOCK_TRANSFER_EMAIL_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_BLOCK_TRANSFER_POST_PIF) {
      detail->type = Registry::PublicRequest::PRT_BLOCK_TRANSFER_POST_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_BLOCK_TRANSFER_GOVERNMENT_PIF) {
      detail->type = Registry::PublicRequest::PRT_BLOCK_TRANSFER_GOVERNMENT_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_UNBLOCK_CHANGES_EMAIL_PIF) {
      detail->type = Registry::PublicRequest::PRT_UNBLOCK_CHANGES_EMAIL_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_UNBLOCK_CHANGES_POST_PIF) {
      detail->type = Registry::PublicRequest::PRT_UNBLOCK_CHANGES_POST_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_UNBLOCK_CHANGES_GOVERNMENT_PIF) {
      detail->type = Registry::PublicRequest::PRT_UNBLOCK_CHANGES_GOVERNMENT_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_UNBLOCK_TRANSFER_EMAIL_PIF) {
      detail->type = Registry::PublicRequest::PRT_UNBLOCK_TRANSFER_EMAIL_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_UNBLOCK_TRANSFER_POST_PIF) {
      detail->type = Registry::PublicRequest::PRT_UNBLOCK_TRANSFER_POST_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_UNBLOCK_TRANSFER_GOVERNMENT_PIF) {
      detail->type = Registry::PublicRequest::PRT_UNBLOCK_TRANSFER_GOVERNMENT_PIF;
  }
  else if (_request->getType() == Corba::Admin::PRT_MOJEID_CONTACT_CONDITIONAL_IDENTIFICATION) {
      detail->type = Registry::PublicRequest::PRT_MOJEID_CONTACT_CONDITIONAL_IDENTIFICATION;
  }
  else if (_request->getType() == Corba::Admin::PRT_MOJEID_CONTACT_IDENTIFICATION) {
      detail->type = Registry::PublicRequest::PRT_MOJEID_CONTACT_IDENTIFICATION;
  }
  else if (_request->getType() == Corba::Admin::PRT_MOJEID_CONTACT_VALIDATION) {
      detail->type = Registry::PublicRequest::PRT_MOJEID_CONTACT_VALIDATION;
  }
  else if (_request->getType() == Corba::Admin::PRT_MOJEID_CONDITIONALLY_IDENTIFIED_CONTACT_TRANSFER) {
      detail->type = Registry::PublicRequest::PRT_MOJEID_CONDITIONALLY_IDENTIFIED_CONTACT_TRANSFER;
  }
  else if (_request->getType() == Corba::Admin::PRT_MOJEID_IDENTIFIED_CONTACT_TRANSFER) {
      detail->type = Registry::PublicRequest::PRT_MOJEID_IDENTIFIED_CONTACT_TRANSFER;
  }
  else if (_request->getType() == Corba::Admin::PRT_MOJEID_CONTACT_REIDENTIFICATION) {
      detail->type = Registry::PublicRequest::PRT_MOJEID_CONTACT_REIDENTIFICATION;
  }
  else if (_request->getType() == Corba::Admin::PRT_MOJEID_CONTACT_PREVALIDATED_UNIDENTIFIED_TRANSFER) {
      detail->type = Registry::PublicRequest::PRT_MOJEID_PREVALIDATED_UNIDENTIFIED_CONTACT_TRANSFER;
  }
  else if (_request->getType() == Corba::Admin::PRT_MOJEID_CONTACT_PREVALIDATED_TRANSFER) {
      detail->type = Registry::PublicRequest::PRT_MOJEID_PREVALIDATED_CONTACT_TRANSFER;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_PERSONALINFO_AUTO_PIF) {
      detail->type = Registry::PublicRequest::PRT_PERSONALINFO_AUTO_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_PERSONALINFO_EMAIL_PIF) {
      detail->type = Registry::PublicRequest::PRT_PERSONALINFO_EMAIL_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_PERSONALINFO_POST_PIF) {
      detail->type = Registry::PublicRequest::PRT_PERSONALINFO_POST_PIF;
  }
  else if (_request->getType() == LibFred::PublicRequest::PRT_PERSONALINFO_GOVERNMENT_PIF) {
      detail->type = Registry::PublicRequest::PRT_PERSONALINFO_GOVERNMENT_PIF;
  }
  else {
      throw std::runtime_error("unknown public request type");
  }

  detail->createTime = DUPSTRDATE(_request->getCreateTime);
  detail->resolveTime = DUPSTRDATE(_request->getResolveTime);
  detail->reason = _request->getReason().c_str();
  detail->email = _request->getEmailToAnswer().c_str();

  detail->answerEmail.id     = _request->getAnswerEmailId();
  detail->answerEmail.handle = DUPSTRC(stringify(_request->getAnswerEmailId()));
  detail->answerEmail.type   = ccReg::FT_MAIL;

  detail->registrar.id     = _request->getRegistrarId();
  detail->registrar.handle = DUPSTRFUN(_request->getRegistrarHandle);
  detail->registrar.type   = ccReg::FT_REGISTRAR;

  unsigned objects_size = _request->getObjectSize();
  detail->objects.length(objects_size);
  for (unsigned i = 0; i < objects_size; ++i) {
    LibFred::PublicRequest::OID oid = _request->getObject(0);
    detail->objects[i].id = oid.id;
    detail->objects[i].handle = oid.handle.c_str();
    switch (oid.type) {
      case LibFred::PublicRequest::OT_DOMAIN:
        detail->objects[i].type = ccReg::FT_DOMAIN;
        break;
      case LibFred::PublicRequest::OT_CONTACT:
        detail->objects[i].type = ccReg::FT_CONTACT;
        break;
      case LibFred::PublicRequest::OT_NSSET:
        detail->objects[i].type = ccReg::FT_NSSET;
        break;
      case LibFred::PublicRequest::OT_KEYSET:
        detail->objects[i].type = ccReg::FT_KEYSET;
        break;

      default:
        FREDLOG_ERROR("Not allowed object type for PublicRequest detail!");
        break;
    }
  }

  return detail;
}


Registry::Invoicing::Detail* ccReg_Session_i::createInvoiceDetail(LibFred::Invoicing::Invoice *_invoice) {
  Registry::Invoicing::Detail *detail = new Registry::Invoicing::Detail();

  detail->id = _invoice->getId();
  detail->zone = _invoice->getZoneId();
  detail->createTime = DUPSTRDATE_NOLTCONVERT(_invoice->getCrDate);
  detail->taxDate = DUPSTRDATED(_invoice->getTaxDate);
  detail->fromDate = DUPSTRDATED(_invoice->getAccountPeriod().begin);
  detail->toDate = DUPSTRDATED(_invoice->getAccountPeriod().end);
  detail->type = (_invoice->getType() == LibFred::Invoicing::IT_DEPOSIT ? Registry::Invoicing::IT_ADVANCE
                                                                         : Registry::Invoicing::IT_ACCOUNT);
  detail->number = DUPSTRC(stringify(_invoice->getPrefix()));
  detail->credit = DUPSTRC(formatMoney(_invoice->getCredit()));
  detail->price = DUPSTRC(formatMoney(_invoice->getPrice()));
  detail->vatRate = DUPSTRC(_invoice->getVat().get_string());
  detail->total = DUPSTRC(formatMoney(_invoice->getTotal()));
  detail->totalVAT = DUPSTRC(formatMoney(_invoice->getTotalVat()));
  detail->varSymbol = DUPSTRC(_invoice->getVarSymbol());

  detail->registrar.id     = _invoice->getRegistrarId();
  detail->registrar.handle = DUPSTRC(_invoice->getClient()->getHandle());
  detail->registrar.type   = ccReg::FT_REGISTRAR;

  detail->filePDF.id     = _invoice->getFileId();
  // detail->filePDF.handle = _invoice->getFileNamePDF().c_str();

  detail->filePDF.handle = DUPSTRC(_invoice->getFileHandle());
  detail->filePDF.type   = ccReg::FT_FILE;

  detail->fileXML.id     = _invoice->getFileXmlId();
  detail->fileXML.handle = DUPSTRC(_invoice->getFileXmlHandle());

  detail->fileXML.type   = ccReg::FT_FILE;

  detail->payments.length(_invoice->getSourceCount());
  for (unsigned n = 0; n < _invoice->getSourceCount(); ++n) {
    const LibFred::Invoicing::PaymentSource *ps = _invoice->getSource(n);
    detail->payments[n].id = ps->getId();
    detail->payments[n].price = DUPSTRC(formatMoney(ps->getPrice()));
    detail->payments[n].balance = DUPSTRC(formatMoney(ps->getCredit()));
    detail->payments[n].number = DUPSTRC(stringify(ps->getNumber()));
  }

  detail->paymentActions.length(_invoice->getActionCount());
  for (unsigned n = 0; n < _invoice->getActionCount(); ++n) {
    const LibFred::Invoicing::PaymentAction *pa = _invoice->getAction(n);
    detail->paymentActions[n].paidObject.id     = pa->getObjectId();
    detail->paymentActions[n].paidObject.handle = DUPSTRFUN(pa->getObjectName);
    detail->paymentActions[n].paidObject.type   = ccReg::FT_DOMAIN;

    detail->paymentActions[n].actionTime = DUPSTRDATE_NOLTCONVERT(pa->getActionTime);
    detail->paymentActions[n].expirationDate = DUPSTRDATED(pa->getExDate);
    detail->paymentActions[n].actionType = pa->getAction();
    detail->paymentActions[n].unitsCount = pa->getUnitsCount();
    detail->paymentActions[n].pricePerUnit = DUPSTRC(formatMoney(pa->getPricePerUnit()));
    detail->paymentActions[n].price = DUPSTRC(formatMoney(pa->getPrice()));
  }

  return detail;

}

Registry::Zone::Detail* ccReg_Session_i::createZoneDetail(LibFred::Zone::Zone* _zone)
{
  FREDLOG_TRACE("[CALL] ccReg_Session_i::createZoneDetail()");

  if (_zone == 0)
  {
      FREDLOG_ERROR("_zone is null");
      throw ccReg::Admin::OBJECT_NOT_FOUND();
  }

  FREDLOG_DEBUG(boost::format("generating zone detail for object id=%1%")
      % _zone->getId());
  Registry::Zone::Detail *detail = new Registry::Zone::Detail();

  detail->id = _zone->getId();
  detail->fqdn = DUPSTRFUN(_zone->getFqdn);
  detail->ex_period_min = _zone->getExPeriodMin();
  detail->ex_period_max = _zone->getExPeriodMax();
  detail->ttl = _zone->getTtl();
  detail->hostmaster = DUPSTRFUN(_zone->getHostmaster);
  detail->refresh = _zone->getRefresh();
  detail->update_retr = _zone->getUpdateRetr();
  detail->expiry = _zone->getExpiry();
  detail->minimum = _zone->getMinimum();
  detail->ns_fqdn = DUPSTRFUN(_zone->getNsFqdn);

  detail->ns.length(_zone->getZoneNsSize());
  for (unsigned i = 0; i < _zone->getZoneNsSize(); i++)
  {
      detail->ns[i].id = _zone->getZoneNs(i)->getId();
      detail->ns[i].fqdn = DUPSTRFUN(_zone->getZoneNs(i)->getFqdn);
      detail->ns[i].addr = DUPSTRFUN(_zone->getZoneNs(i)->getAddrs);
  }

  return detail;
}

void ccReg_Session_i::setHistory(CORBA::Boolean _flag)
{
  FREDLOG_SET_CONTEXT(LogContext, ctx, base_context_);
  ConnectionReleaser releaser;

  if (_flag)
  {
    settings_.set("filter.history", "on");
  }
  else
  {
    settings_.set("filter.history", "off");
  }
}
