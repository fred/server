/*
 * Copyright (C) 2008-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef COMMON_HH_356FC41370474BDA82802F9A41BFD04F
#define COMMON_HH_356FC41370474BDA82802F9A41BFD04F

#include "corba/Admin.hh"

#include "src/deprecated/libfred/registry.hh"
#include "src/deprecated/libfred/invoicing/invoice.hh"
#include "src/util/types/money.hh"

#include <boost/date_time/gregorian/gregorian.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <math.h>
#include <string>

std::string formatDate(boost::gregorian::date d);
std::string formatTime(boost::posix_time::ptime p, bool date, bool _to_local = false);
std::string formatMoney(Money m);
boost::posix_time::ptime makeBoostTime(const ccReg::DateTimeType& t);
boost::gregorian::date makeBoostDate(const ccReg::DateType& t);
boost::gregorian::date makeBoostDate_throw(const ccReg::DateType& t);
ccReg::DateTimeType makeCorbaTime(boost::posix_time::ptime p, bool _to_local = false);
ccReg::DateType makeCorbaDate(const boost::gregorian::date& p);
boost::posix_time::time_period setPeriod(const ccReg::DateTimeInterval& _v);
boost::posix_time::time_period setPeriod(const ccReg::DateInterval& _v);
void clearPeriod(ccReg::DateTimeInterval& _v);

#define DUPSTR(s) CORBA::string_dup(s)
#define DUPSTRC(s) CORBA::string_dup(s.c_str())
#define DUPSTRFUN(f) DUPSTRC(f())
#define DUPSTRDATE(f) DUPSTRC(formatTime(f(),true,true))
#define DUPSTRDATE_NOLTCONVERT(f) DUPSTRC(formatTime(f(),true,false))
#define DUPSTRDATED(f) DUPSTRC(formatDate(f()))
#define DUPSTRDATESHORT(f) DUPSTRC(formatTime(f(),false,true))
#define COLHEAD(x,i,title,tp) \
  (*x)[i].name = DUPSTR(title); \
  (*x)[i].type = Registry::Table::tp 

#define FILTER_IMPL(FUNC,PTYPEGET,PTYPESET,MEMBER,MEMBERG, SETF) \
PTYPEGET FUNC() { return MEMBERG; } \
void FUNC(PTYPESET _v) { MEMBER = _v; SETF; }

#define FILTER_IMPL_L(FUNC,MEMBER,SETF) \
  FILTER_IMPL(FUNC,ccReg::TID,ccReg::TID,MEMBER,MEMBER, SETF)

#define FILTER_IMPL_S(FUNC,MEMBER,SETF) \
  FILTER_IMPL(FUNC,char *,const char *,MEMBER,DUPSTRC(MEMBER), SETF)

// #define DUPSTRDATE(f) DUPSTR(to_simple_string(f()).c_str())

#define C_STR(_val)  str_corbaout(_val).c_str()

/* detail library -> corba mappings macros */
#define MAKE_OID(_name, _id, _handle, _type) \
Registry::OID _name;                         \
_name.id     = _id;                          \
_name.handle = _handle;                      \
_name.type   = ccReg::_type

#define CHANGED(method) (act->method() != prev->method()) || (act == prev)

#define ADD_NEW_HISTORY_RECORD(_field, _value)                                         \
do {                                                                                   \
unsigned i = detail->_field.length();                                                  \
detail->_field.length(i + 1);                                                          \
detail->_field[i].value  <<= _value;                                                   \
detail->_field[i].requestId = act->getRequestId();                                     \
detail->_field[i].from     = makeCorbaTime(act->getRequestStartTime(), true);          \
detail->_field[i].to       = (i > 0 ? makeCorbaTime(prev->getRequestStartTime(), true) \
                                    : makeCorbaTime(boost::posix_time::ptime(boost::posix_time::not_a_date_time))); \
} while (false)

#define MODIFY_LAST_HISTORY_RECORD(_field)                                            \
do {                                                                                  \
unsigned i = detail->_field.length();                                                 \
detail->_field[i - 1].requestId = act->getRequestId();                                \
detail->_field[i - 1].from     = makeCorbaTime(act->getRequestStartTime(), true);     \
} while (false)

#define MAP_HISTORY_VARIABLE(_field, _method, _conv)                                  \
do {                                                                                  \
if (CHANGED(_method)) {                                                               \
  ADD_NEW_HISTORY_RECORD(_field, _conv(act->_method()));                              \
}                                                                                     \
else {                                                                                \
  MODIFY_LAST_HISTORY_RECORD(_field);                                                 \
}                                                                                     \
} while (false)

#define MAP_HISTORY_BOOL(_field, _method)                                             \
do {                                                                                  \
if (CHANGED(_method)) {                                                               \
  ADD_NEW_HISTORY_RECORD(_field, CORBA::Any::from_boolean(act->_method()));           \
}                                                                                     \
else {                                                                                \
  MODIFY_LAST_HISTORY_RECORD(_field);                                                 \
}                                                                                     \
} while (false)

#define MAP_HISTORY_OID(_field, _get_id, _get_handle, _type)                          \
do {                                                                                  \
if (CHANGED(_get_id)) {                                                               \
  Registry::OID oid;                                                                  \
  oid.id = act->_get_id();                                                            \
  oid.handle = DUPSTRFUN(act->_get_handle);                                           \
  oid.type = _type;                                                                   \
  ADD_NEW_HISTORY_RECORD(_field, oid);                                                \
}                                                                                     \
else {                                                                                \
  MODIFY_LAST_HISTORY_RECORD(_field);                                                 \
}                                                                                     \
} while (false)

#define MAP_HISTORY_STRING(_field, _method)    MAP_HISTORY_VARIABLE(_field, _method, C_STR)
#define MAP_HISTORY_DATE(_field, _method)      MAP_HISTORY_VARIABLE(_field, _method, C_STR)
#define MAP_HISTORY_DATETIME(_field, _method)  MAP_HISTORY_VARIABLE(_field, _method, C_STR)

#endif//COMMON_HH_356FC41370474BDA82802F9A41BFD04F
