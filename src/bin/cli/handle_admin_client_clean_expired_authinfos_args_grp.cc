/*
 * Copyright (C) 2022  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "src/bin/cli/handle_adminclientselection_args.hh"

#include <boost/program_options.hpp>
#include <boost/date_time/gregorian/greg_date.hpp>

CommandDescription HandleAdminClientCleanExpiredAuthinfosArgsGrp::get_command_option()
{
    return CommandDescription{"clean_expired_authinfos"};
}

std::shared_ptr<boost::program_options::options_description> HandleAdminClientCleanExpiredAuthinfosArgsGrp::get_options_description()
{
    auto cfg_opts = std::make_shared<boost::program_options::options_description>("Command for cleaning of expired authinfos", 120);
    cfg_opts->add_options()
            ("clean_expired_authinfos", "clean all expired authinfos");
    return cfg_opts;
}

std::size_t HandleAdminClientCleanExpiredAuthinfosArgsGrp::handle(int argc, char* argv[], FakedArgs& fa, std::size_t option_group_index)
{
    boost::program_options::variables_map vm;
    handler_parse_args{}(this->get_options_description(), vm, argc, argv, fa);
    return option_group_index;
}
