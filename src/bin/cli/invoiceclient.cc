/*
 * Copyright (C) 2008-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <math.h>
#include "src/bin/cli/commonclient.hh"
#include "src/bin/cli/invoiceclient.hh"
#include "src/deprecated/libfred/credit.hh"
#include "src/deprecated/libfred/invoicing/invoice.hh"
#include "src/util/types/money.hh"
namespace Admin {



void
InvoiceClient::runMethod()
{
    if (invoice_credit) {
        credit();
    } else if (invoice_billing) {
        billing();
    } else if (invoice_add_prefix) {
        add_invoice_prefix();
    }
}


void
InvoiceClient::credit()
{
    std::unique_ptr<LibFred::Invoicing::Manager>
        invMan(LibFred::Invoicing::Manager::create());

    boost::posix_time::ptime local_current_timestamp
        = boost::posix_time::microsec_clock::local_time();

    unsigned long long zoneId = 0;
    if (credit_params.zone_id.is_value_set())
    {
        zoneId = credit_params.zone_id.get_value();
    }
    else
    {
        throw std::runtime_error("Zone id is not set, use --zone_id to set it ");
    }

    unsigned long long regId = 0;
    if (credit_params.registrar_id.is_value_set())
    {
        regId = credit_params.registrar_id.get_value();
    }
    else
    {
        throw std::runtime_error("Registrar is not set, use ``--registrar_id'' to set it");
    }

    Money price;
    if (credit_params.price.is_value_set())
    {
        price = credit_params.price.get_value();
    }
    else
    {
        throw std::runtime_error("Price is not set, use ``--price'' to set it");
    }

    boost::gregorian::date taxDate;
    if (credit_params.taxdate.is_value_set())
    {
        taxDate = boost::gregorian::from_string(credit_params.taxdate.get_value());
    }
    else
    {
        taxDate = local_current_timestamp.date();
    }

    Database::Connection conn = Database::Manager::acquire();
    Database::Transaction tx(conn);

    Money out_credit;
    unsigned long long invoice_id
        = invMan->createDepositInvoice(taxDate, zoneId, regId, price, local_current_timestamp, out_credit);

    LibFred::Credit::add_credit_to_invoice( regId,  zoneId, out_credit, invoice_id);

    tx.commit();
}


void
InvoiceClient::billing()
{
    //zone
    std::string zone_name;
    if (billing_params.zone_fqdn.is_value_set())
    {
        zone_name = billing_params.zone_fqdn.get_value();
    }
    else
    {
        throw std::runtime_error("billing: zone_fqdn not set");
    }

    //registrar
    std::string registrar_name;
    if (billing_params.registrar_handle.is_value_set())
    {
        registrar_name = billing_params.registrar_handle.get_value();
    }

    //fromdate
    boost::gregorian::date fromdate;
    if (billing_params.fromdate.is_value_set())
    {
        fromdate = boost::gregorian::from_simple_string(billing_params.fromdate.get_value());

        if(fromdate.is_special())
        {
            std::cerr << "Invalid date given for ``fromdate'' " << std::endl;
            throw std::runtime_error("billing: invalid fromdate set");
        }
    }//not set fromdate is default

    //todate
    boost::gregorian::date todate;
    if (billing_params.todate.is_value_set())
    {
        todate = boost::gregorian::from_simple_string(billing_params.todate.get_value());

        if(todate.is_special())
        {
            std::cerr << "Invalid date given for ``todate'' " << std::endl;
            throw std::runtime_error("billing: invalid todate set");
        }
    }
    else
    {
        boost::gregorian::date tmp_today
            = boost::posix_time::second_clock::local_time().date();
        todate = boost::gregorian::date(tmp_today.year()
            , tmp_today.month(), 1);//first day of current month
    }

    //taxdate
    boost::gregorian::date taxdate;
    if (billing_params.taxdate.is_value_set())
    {
        taxdate = boost::gregorian::from_simple_string(billing_params.taxdate.get_value());

        if(taxdate.is_special())
        {
            std::cerr << "Invalid date given for ``taxdate'' " << std::endl;
            throw std::runtime_error("billing: invalid taxdate set");
        }
    }
    else
    {
        taxdate = todate - boost::gregorian::days(1);//last day of last month
    }

    //invoicedate
    boost::posix_time::ptime invoicedate;
    if (billing_params.invoicedate.is_value_set())
    {
        invoicedate = boost::posix_time::time_from_string(billing_params.invoicedate.get_value());
        if (invoicedate.is_special())
        {
            std::cerr << "Invalid date given for ``invoicedate'' " << std::endl;
            throw std::runtime_error("billing: invalid invoicedate set");
        }
    }
    else
    {
        invoicedate = boost::posix_time::microsec_clock::local_time();
    }

    FREDLOG_DEBUG ( boost::format("InvoiceClient::billing"
            " zonename %1%  registrarname %2% taxdate %3%  todate (not included) %4% invoicedate %5%")
    % zone_name % registrar_name
    % boost::gregorian::to_simple_string(taxdate)
    % boost::gregorian::to_simple_string(todate)
    % boost::posix_time::to_simple_string(invoicedate));

    //conversion from command line params into implementation params
    todate -= boost::gregorian::days(1); //today from date after range into last day of range

    std::unique_ptr<LibFred::Invoicing::Manager>
        invMan(LibFred::Invoicing::Manager::create());

    if (!billing_params.registrar_handle.is_value_set()) {
        invMan->createAccountInvoices( zone_name
                , taxdate, fromdate, todate, invoicedate);
    }
    else
    {
        invMan->createAccountInvoice( registrar_name, zone_name
                , taxdate, fromdate, todate, invoicedate);
    }
}

void
InvoiceClient::add_invoice_prefix()
{
    std::unique_ptr<LibFred::Invoicing::Manager>
        invMan(LibFred::Invoicing::Manager::create());

    unsigned int type = prefix_params.type.get_value();
    if (type > 1) {
        std::cerr << "Type can be either 0 or 1." << std::endl;
        return;
    }
    unsigned int year;
    if (prefix_params.year.is_value_set()) {
        year = prefix_params.year.get_value();
    } else {
        Database::Date now(Database::NOW);
        year = now.get().year();
    }
    unsigned long long prefix = prefix_params.prefix.get_value();
    if (prefix_params.zone_id.is_value_set()) {
        unsigned int zoneId = prefix_params.zone_id.get_value();
        invMan->insertInvoicePrefix(zoneId, type, year, prefix);
    } else if (prefix_params.zone_fqdn.is_value_set()) {
        std::string zoneName = prefix_params.zone_fqdn.get_value();
        invMan->insertInvoicePrefix(zoneName, type, year, prefix);
    }
}

void create_invoice_prefixes(CreateInvoicePrefixesArgs params)
{
    std::unique_ptr<LibFred::Invoicing::Manager>
        invMan(LibFred::Invoicing::Manager::create());
    invMan->createInvoicePrefixes(params.for_current_year);
}

void add_invoice_number_prefix(AddInvoiceNumberPrefixArgs params)
{
    std::unique_ptr<LibFred::Invoicing::Manager>
        invMan(LibFred::Invoicing::Manager::create());
    invMan->addInvoiceNumberPrefix(
            params.prefix
            , params.zone_fqdn
            , params.invoice_type_name
            );
}

} // namespace Admin;
