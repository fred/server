/*
 * Copyright (C) 2011-2023  CZ.NIC, z. s. p. o.
 *
 * This file is part of FRED.
 *
 * FRED is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * FRED is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with FRED.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef INVOICE_CLIENT_IMPL_HH_6E4EE876C9984B8EBD641AD90656A56C
#define INVOICE_CLIENT_IMPL_HH_6E4EE876C9984B8EBD641AD90656A56C

#include "src/util/cfg/config_handler_decl.hh"
#include "src/util/cfg/handle_database_args.hh"
#include "src/util/cfg/handle_corbanameservice_args.hh"
#include "src/util/cfg/handle_registry_args.hh"
#include "src/bin/cli/handle_adminclientselection_args.hh"
#include "src/bin/cli/invoiceclient.hh"

#include "util/log/log.hh"

/**
 * \class invoice_credit_impl
 * \brief admin client implementation of invoice_credit
 */
struct invoice_credit_impl
{
  void operator()() const
  {
      FREDLOG_SET_CONTEXT(LogContext, ctx, "invoice_credit_impl");
      Admin::InvoiceClient invoice_client(
              CfgArgGroups::instance()->get_handler_ptr_by_type<HandleDatabaseArgsGrp>()->get_conn_info()
              , true//bool _invoice_credit
              , false//bool _invoice_billing
              , false//bool _invoice_add_prefix
              , false//bool _invoice_create
              , false//bool _invoice_dont_send
              , CfgArgGroups::instance()->get_handler_ptr_by_type<HandleAdminClientInvoiceCreditArgsGrp>()->params//InvoiceCreditArgs()
              , InvoiceBillingArgs()
              , InvoicePrefixArgs()
              );
      invoice_client.runMethod();
  }
};

/**
 * \class invoice_billing_impl
 * \brief admin client implementation of invoice_billing
 */
struct invoice_billing_impl
{
  void operator()() const
  {
      FREDLOG_SET_CONTEXT(LogContext, ctx, "invoice_billing_impl");
      Admin::InvoiceClient invoice_client(
              CfgArgGroups::instance()->get_handler_ptr_by_type<HandleDatabaseArgsGrp>()->get_conn_info()
              , false//bool _invoice_credit
              , true//bool _invoice_billing
              , false//bool _invoice_add_prefix
              , false//bool _invoice_create
              , false//bool _invoice_dont_send
              , InvoiceCreditArgs()
              , CfgArgGroups::instance()->get_handler_ptr_by_type<HandleAdminClientInvoiceBillingArgsGrp>()->params//InvoiceBillingArgs()
              , InvoicePrefixArgs()
              );
      invoice_client.runMethod();
  }
};

/**
 * \class invoice_add_prefix_impl
 * \brief admin client implementation of invoice_add_prefix
 */
struct invoice_add_prefix_impl
{
  void operator()() const
  {
      FREDLOG_SET_CONTEXT(LogContext, ctx, "invoice_add_prefix_impl");
      Admin::InvoiceClient invoice_client(
              CfgArgGroups::instance()->get_handler_ptr_by_type<HandleDatabaseArgsGrp>()->get_conn_info()
              , false//bool _invoice_credit
              , false//bool _invoice_billing
              , true//bool _invoice_add_prefix
              , false//bool _invoice_create
              , false//bool _invoice_dont_send
              , InvoiceCreditArgs()
              , InvoiceBillingArgs()
              , CfgArgGroups::instance()->get_handler_ptr_by_type<HandleAdminClientInvoiceAddPrefixArgsGrp>()->params//InvoicePrefixArgs()
              );
      invoice_client.runMethod();
  }
};

/**
 * \class create_invoice_prefixes_impl
 * \brief admin client implementation of create_invoice_prefixes_impl
 */
struct create_invoice_prefixes_impl
{
  void operator()() const
  {
      FREDLOG_SET_CONTEXT(LogContext, ctx, "create_invoice_prefixes_impl");
      Admin::create_invoice_prefixes(CfgArgGroups::instance()
      ->get_handler_ptr_by_type
          <HandleAdminClientCreateInvoicePrefixesArgsGrp>()
      ->params);
  }
};

/**
 * \class add_invoice_number_prefix_impl
 * \brief admin client implementation of add_invoice_number_prefix_impl
 */
struct add_invoice_number_prefix_impl
{
  void operator()() const
  {
      FREDLOG_SET_CONTEXT(LogContext, ctx, "add_invoice_number_prefix_impl");

      Admin::add_invoice_number_prefix(CfgArgGroups::instance()
          ->get_handler_ptr_by_type
              <HandleAdminClientAddInvoiceNumberPrefixArgsGrp>()
          ->params);
  }
};


/**
 * \class create_invoiceimpl
 * \brief admin client implementation of create_invoice
 */
struct create_invoice_impl
{
  void operator()() const
  {
      FREDLOG_SET_CONTEXT(LogContext, ctx, "create_invoice_impl");
      Admin::InvoiceClient invoice_client(
              CfgArgGroups::instance()->get_handler_ptr_by_type<HandleDatabaseArgsGrp>()->get_conn_info()
              , false//bool _invoice_credit
              , false//bool _invoice_billing
              , false//bool _invoice_add_prefix
              , true//bool _invoice_create
              , false//bool _invoice_dont_send
              , InvoiceCreditArgs()
              , InvoiceBillingArgs()
              , InvoicePrefixArgs()
              );
      invoice_client.runMethod();
  }
};

#endif
